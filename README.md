ONAP-TESTS
==========

Description
-----------

This repository provides different tools to test your ONAP platform.

The reference heat, env and Tosca files are by default related to the Orange
ONAP OpenLab (<https://wiki.onap.org/display/DW/Orange+OpenLab>).

You obviously must adapt the different configurations (env/proxy/IP) according
to your environment.

The Python framework <https://gitlab.com/Orange-OpenSource/onap-tests/tree/master/onap_tests>
has been designed to automate VNF onboarding and instantiation.

Official framework based on RobotFramework may be found on ONAP web site.

This python package is not an official project within ONAP but can be used to
learn and automate VNFs for CI chains.

Several examples are provided:

* freeradius
* clearwater IMS
* ansible server
* canonic vFW

Help us and contribute
----------------------

This gitlab is public. Everyone is invited to contribute.

Contact us
----------

Send us a mail at onap-openlab AT list.orange.com or use directly the different
gitlab functions.
