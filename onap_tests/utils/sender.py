#!/usr/bin/python
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
#  pylint: disable=missing-docstring
#  pylint: disable=too-many-arguments
#  pylint: disable=too-many-locals
#  pylint: disable=no-self-use
"""Sender class."""
import logging
import requests
from requests.adapters import HTTPAdapter
import urllib3
from urllib3.util.retry import Retry

import simplejson.errors
import onap_tests.utils.utils as onap_utils


urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

PROXY = onap_utils.get_config("general.proxy")


class Sender():
    """Sender Object.

    Wrapper around requests in order to have consistent calls and consistent
    call treatment.
    """

    __logger = logging.getLogger(__name__)

    def requests_retry_session(self, retries=10,
                               backoff_factor=0.3, session=None):
        """Request with a retry."""
        session = session or requests.Session()
        retry = Retry(
            total=retries,
            read=retries,
            connect=retries,
            backoff_factor=backoff_factor,
        )
        adapter = HTTPAdapter(max_retries=retry)
        session.mount('http://', adapter)
        session.mount('https://', adapter)
        return session

    def send_message(self, server, method, action, url, header, **kwargs):
        """
        Send a message to an HTTP server.

        :param method: which method to use (GET, POST, PUT, PATCH, ...)
        :param server: nickname of the server we send the request.
                used in logs strings.
        :param action: what action are we doing, used in logs strings.
        :param url: the url to use
        :param header: the headers to use
        :param data: (optional) Dictionary, list of tuples, bytes,
            or file-like object to send in the body.
        :param files: (optional) Dictionary, list of tuples, bytes,
                or file-like object to send in the body.
        :param exception: (optional) if an error occurs, raise the
                exception given

        :return: the response of the request or None if there was an Error
        """
        exception = kwargs.pop('exception', None)
        debug = kwargs.pop('debug', False)
        data = kwargs.get('data', None)
        try:
            # build the request with the requested method
            session = self.requests_retry_session()
            response = session.request(method, url, headers=header,
                                       verify=False, proxies=PROXY, **kwargs)

            response.raise_for_status()
            self.__logger.info("[%s][%s] response code: %s", server, action,
                               response.status_code)
            if debug:
                self.__logger.debug("[%s][%s] sent header: %s",
                                    server, action, header)
                self.__logger.debug("[%s][%s] url used: %s", server, action,
                                    url)
                self.__logger.debug("[%s][%s] data sent: %s", server, action,
                                    data)
                self.__logger.debug("[%s][%s] response: %s",
                                    server, action, response.text)
            return response
        except requests.HTTPError:
            self.__logger.error(
                "[%s][%s] response code: %s",
                server, action, response.status_code)
            self.__logger.error("[%s][%s] sent header: %s",
                                server, action, header)
            self.__logger.error("[%s][%s] url used: %s", server, action, url)
            self.__logger.error("[%s][%s] data sent: %s", server, action, data)
            self.__logger.error("[%s][%s] response: %s",
                                server, action, response.text)
            if exception:
                raise exception
        except requests.RequestException as err:
            self.__logger.error("[%s][%s]Failed to perform: %s",
                                server, action, err)
            self.__logger.error("[%s][%s] sent header: %s",
                                server, action, header)
            self.__logger.error("[%s][%s] url used: %s", server, action, url)
            self.__logger.error("[%s][%s] data sent: %s", server, action, data)
            if exception:
                raise exception
        return None

    def send_message_json(self, server, method, action, url, header, **kwargs):
        """
        Send a message to an HTTP server and parse the response as JSON.

        :param method: which method to use (GET, POST, PUT, PATCH, ...)
        :param server: nickname of the server we send the request.
            used in logs strings.
        :param action: what action are we doing, used in logs strings.
        :param url: the url to use
        :param header: the headers to use
        :param data: (optional) Dictionary, list of tuples, bytes, or file-like
            object to send in the body.
        :param exception: (optional) if an error occurs, raise the exception
            given
        :param debug: (optional) if set, will show what has been sent and
            received

        :return: the response of the request or None if there was an Error
        """
        exception = kwargs.get('exception', None)
        data = kwargs.get('data', None)
        try:
            response = self.send_message(server, method, action, url,
                                         header, **kwargs)
            if response:
                return response.json()
        except simplejson.errors.JSONDecodeError as err:
            self.__logger.error("[%s][%s]Failed to decode JSON: %s",
                                server, action, err)
            self.__logger.error("[%s][%s] sent header: %s",
                                server, action, header)
            self.__logger.error("[%s][%s] url used: %s", server, action, url)
            self.__logger.error("[%s][%s] data sent: %s", server, action, data)
            if exception:
                raise exception
        return {}
