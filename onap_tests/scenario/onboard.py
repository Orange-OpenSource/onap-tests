#!/usr/bin/python
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
#  pylint: disable=missing-docstring
#  pylint: disable=too-many-branches
#  pylint: disable=too-many-statements
#  pylint: disable=too-many-instance-attributes
#  pylint: disable=invalid-name
"""Onboard class."""
import copy
import logging
import sys
import time

import onap_tests.components.vendor as vendor
import onap_tests.components.vl as vl
import onap_tests.components.vsp as vsp
import onap_tests.components.vfc as vfc
import onap_tests.components.vf as vfunc
import onap_tests.components.vfvfc as vfuncvfc
import onap_tests.components.service as service
import onap_tests.components.initonapdata as initonapdata
import onap_tests.utils.utils as onap_utils

PROXY = onap_utils.get_config("general.proxy")


class Onboard():
    """Onboard classe to perform SDC operations."""

    __logger = logging.getLogger(__name__)

    def __init__(self, **kwargs):
        """Initialize Onboard object."""
        super(Onboard, self).__init__()
        self.vendor = vendor.Vendor(**kwargs)
        self.vsp = vsp.VSP(**kwargs)
        self.vfc = vfc.VFC(**kwargs)
        self.vsp_list = []
        self.vfc_list = []
        self.vl = vl.VL(**kwargs)
        self.vl_list = []
        self.vfunc = vfunc.VF(**kwargs)
        self.vfunc_list = []
        self.vfuncvfc = vfuncvfc.Vfvfc(**kwargs)
        self.vfuncvfc_list = []
        self.service = service.Service(**kwargs)
        self.initonapdata = initonapdata.InitOnapData(**kwargs)
        # added for onboard_vfc
        if "service_name" in kwargs:
            self.service_name = kwargs['service_name']
        else:
            self.service_name = "ONAP_Tests_default"

    def onboard_vendor(self):
        """Onboard vendor."""
        # 1) vendor creation
        vendor_exists = self.vendor.check_vendor_exists()
        if self.vendor.status != "Certified":
            if vendor_exists:
                self.__logger.info(
                    "%s vendor already exists: skip creation",
                    self.vendor.name)
            else:
                self.__logger.info("Create vendor")
                self.vendor.create_vendor()
                self.__logger.info("Check and update vendor")
                self.vendor.check_vendor_exists()
            # # 2) Checkin
            # if (self.vendor.status == "Final" or
            #         self.vendor.status == "Available"):
            #     self.__logger.info(
            #         "State is Final or Available, skip checkin")
            # else:
            #     self.__logger.info(
            #         "Vendor status: %s: vendor checkin",
            #         self.vendor.status)
            #     self.__logger.info(
            #         "Check and update vendor")
            #     self.vendor.checkin_vendor()

            # 3) Submit
            if self.vendor.status == "Certified":
                self.__logger.info(
                    "State is Certified, skip submit")
            else:
                self.__logger.info("vendor submit")
                self.vendor.submit_vendor()
                self.__logger.info("Check and update vendor")
                self.vendor.check_vendor_exists()
        else:
            self.__logger.info("vendor already in Certified status")

    def check_vendor_prerequisites(self):
        """
        Process pre-requisites.

          * Vendor must exists
          * Vendor information must be loaded
        """
        onboard_prerequisites = False
        if self.vendor.check_vendor_exists():
            self.__logger.info("Check and update vendor info")
            onboard_prerequisites = True
        else:
            self.__logger.info(
                "Vendor does not exists : stop VSP onboarding")
        return onboard_prerequisites

    def onboard_vfc(self):
        """Onboard VFC."""
        service_params = onap_utils.get_service_custom_config(
            self.service.name)
        if "vnfcs" in service_params:
            for vnfc in service_params['vnfcs']:
                self.vfc.update_vfc(name=vnfc['vnf_name'] + "_VFC")
                self.__logger.info("Start creation VFC %s", self.vfc.name)
                vfc_exists = self.vfc.check_vfc_exists()
                if self.vfc.status != "CERTIFIED":
                    # 1a) VFC creation
                    if vfc_exists:
                        self.__logger.info(
                            "%s VFC already exists: skip creation",
                            self.vfc.name)
                    else:
                        self.__logger.info("Create VFC %s", self.vfc.name)
                        self.vfc.create_vfc(self.vendor)

                        # 1b) Checkin
                        if self.vfc.status in ["CERTIFIED",
                                               "CERTIFICATION_IN_PROGRESS",
                                               "READY_FOR_CERTIFICATION",
                                               "NOT_CERTIFIED_CHECKIN"]:
                            self.__logger.info("already done, skip checkin")
                        else:
                            self.__logger.info("vfc checkin %s", self.vfc.name)
                            self.vfc.checkin_vfc()

                    # 2) Submit
                    self.__logger.info("vfc submit-vfc status %s",
                                       self.vfc.status)
                    if self.vfc.status == "CERTIFIED":
                        self.__logger.info("State is Certified, skip submit")
                    else:
                        self.__logger.info("vfc submit")
                        self.vfc.submit_for_testing_vfc()

                    # 3) Start Certification
                    if self.vfc.status in ["CERTIFIED",
                                           "CERTIFICATION_IN_PROGRESS"]:
                        self.__logger.info(
                            "already done, skip Start Certification")
                    else:
                        self.__logger.info("vfc Start Certification")
                        self.vfc.start_certif_vfc()

                    # 4) Certify
                    if self.vfc.status == "CERTIFIED":
                        self.__logger.info("already done, skip Certify")
                    else:
                        self.__logger.info("vfc_exists certify")
                        self.vfc.certify_vfc()
                        self.__logger.info("Check and update VFC")
                        self.vfc.check_vfc_exists()
                        self.vfc_list.append(copy.copy(self.vfc))

                else:
                    self.__logger.info(
                        "Vfc %s already Certified : skipping vfc operations",
                        self.vfc.name)
                    self.vfc_list.append(copy.copy(self.vfc))

    def onboard_vsp(self):
        """Onboard VSP."""
        service_params = onap_utils.get_service_custom_config(
            self.service.name)
        if "vnfs" in service_params:
            for vnf in service_params['vnfs']:
                self.vsp.update_vsp(name=vnf['vnf_name'] + "_VSP")
                self.__logger.info("Debut creation VSP %s", self.vsp.name)
                vsp_exists = self.vsp.check_vsp_exists()   # # 2) Checkin
                # if (self.vendor.status == "Final" or
                #         self.vendor.status == "Available"):
                #     self.__logger.info(
                #         "State is Final or Available, skip checkin")
                # else:
                #     self.__logger.info(
                #         "Vendor status: %s: vendor checkin",
                #         self.vendor.status)
                #     self.__logger.info(
                #         "Check and update vendor")
                #     self.vendor.checkin_vendor()
                if self.vsp.status != "Certified":
                    if self.check_vendor_prerequisites():
                        # 1) VSP creation
                        if vsp_exists:
                            self.__logger.info(
                                "%s VSP already exists: skip creation",
                                self.vsp.name)
                        else:
                            self.__logger.info("Create VSP %s", self.vsp.name)
                            self.vsp.create_vsp(self.vendor)
                            self.__logger.info("Check and update VSP")
                            self.vsp.check_vsp_exists()

                        # 2) upload heat files
                        self.__logger.info(
                            "upload Heat files to VSP %s", self.vsp.name)
                        heat_file_path = (sys.path[-1] +
                                          "/onap_tests/templates/heat_files/" +
                                          vnf['heat_files_to_upload'])
                        onap_utils.create_zip_dir(heat_file_path)
                        files = {'upload': open(heat_file_path, 'rb')}
                        self.vsp.upload_vsp(files)
                        self.__logger.info("Check and update VSP")
                        self.vsp.check_vsp_exists()

                        # 3) Validate
                        # Verify the different status
                        if self.vsp.status == "Certified":
                            self.__logger.info(
                                "State is Certified, skip validate")
                        else:
                            self.__logger.info("validate Heat files")
                            self.vsp.validate_vsp()
                            self.__logger.info("Check and update VSP")
                            self.vsp.check_vsp_exists()

                        # 4) Commit
                        if self.vsp.status == "Certified":
                            self.__logger.info(
                                "State is Certified, skip Commit")
                        else:
                            self.__logger.info("vsp commit")
                            self.vsp.commit_vsp()
                            self.__logger.info("Check and update VSP")
                            self.vsp.check_vsp_exists()

                        # 5) Submit
                        if self.vsp.status == "Certified":
                            self.__logger.info(
                                "State is Certified, skip submit")
                        else:
                            self.__logger.info("vsp submit")
                            self.vsp.submit_vsp()
                            self.__logger.info("Check and update VSP")
                            self.vsp.check_vsp_exists()

                        # 6) Create package
                        self.__logger.info("Create CSAR package")
                        self.vsp.create_package_vsp()
                        self.__logger.info("Check and update VSP")
                        self.vsp.check_vsp_exists()
                        self.vsp_list.append(copy.copy(self.vsp))
                else:
                    self.__logger.info(
                        "VSP %s already Certified : skipping vsp operations",
                        self.vsp.name)
                    self.__logger.info("Create CSAR package")
                    self.vsp.create_package_vsp()
                    self.__logger.info("Check and update VSP")
                    self.vsp.check_vsp_exists()
                    self.vsp_list.append(copy.copy(self.vsp))

    def check_vf_prerequisites(self):
        """
        Process pre-requisites.

          * VSP must exists
          * VSP information must be loaded
        """
        onboard_prerequisites = False
        if self.vsp.check_vsp_exists():
            self.__logger.info("Check and update VSP info")
            onboard_prerequisites = True
        else:
            self.__logger.info(
                "VSP %s does not exists : stop VF onboarding",
                self.vsp.name)
        return onboard_prerequisites

    def check_vfvfc_prerequisites(self):
        """
        Process pre-requisites.

        VFC must exists
        VFC information must be loaded
        """
        onboard_prerequisites = False
        if self.vfc.check_vfc_exists():
            self.__logger.info("Check and update VFC info")
            onboard_prerequisites = True
        else:
            self.__logger.info(
                "VFC %s does not exists : stop VF onboarding",
                self.vfc.name)
        return onboard_prerequisites

    def onboard_vf(self):
        """Onboard VF."""
        service_params = onap_utils.get_service_custom_config(
            self.service.name)
        if "vnfs" in service_params:
            for idx, vnf in enumerate(service_params['vnfs']):
                self.vfunc.update_vf(name=vnf['vnf_name'] + "_VF")
                vf_exists = self.vfunc.check_vf_exists()
                prerequisites = self.check_vf_prerequisites()
                if self.vfunc.status != "CERTIFIED":
                    if prerequisites:
                        # 1) VF creation
                        if vf_exists:
                            self.__logger.info(
                                "%s VF already exists: skip creation",
                                self.vfunc.name)
                        else:
                            self.__logger.info(
                                "Create VF %s",
                                self.vfunc.name)
                            self.__logger.info(
                                "VF using VSP name %s",
                                self.vsp_list[idx].name)
                            self.__logger.info(
                                "VF using VSP id %s",
                                self.vsp_list[idx].id)
                            self.__logger.info(
                                "VF using VSP csar_uuid %s",
                                self.vsp_list[idx].csar_uuid)
                            self.vfunc.create_vf(self.vsp_list[idx])
                            self.vfunc.posx = 200 + 100 * idx
                            self.vfunc.posy = 200 + 100 * idx
                            self.__logger.info(
                                "Check and update VF %s", self.vfunc.name)
                            self.vfunc.check_vf_exists()

                        # 3) Certify
                        if self.vfunc.status == "CERTIFIED":
                            self.__logger.info("already done, skip Certify")
                        else:
                            self.__logger.info("vf certify")
                            self.vfunc.certify_vf()
                            self.__logger.info("Check and update VF")
                            self.vfunc.check_vf_exists()
                else:
                    self.__logger.info(
                        "vf already Certified : skipping operations")

                # 7) update vf unique_id
                self.__logger.info("vf unique_id update")
                self.vfunc.get_vf_metadata()

                self.vfunc_list.append(copy.copy(self.vfunc))

    def onboard_vf_with_vfc(self):
        """Onboard VF from VFC component."""
        service_params = onap_utils.get_service_custom_config(
            self.service.name)
        for idx, vnf in enumerate(service_params['vnfcs']):
            self.vfuncvfc.update_vf(name=vnf['vnf_name'] + "_VFC_VF")
            vf_exists = self.vfuncvfc.check_vf_exists()
            prerequisites = self.check_vfvfc_prerequisites()
            if self.vfuncvfc.status != "CERTIFIED":
                if prerequisites:
                    # 1) VF creation
                    if vf_exists:
                        self.__logger.info(
                            "%s VF already exists: skip creation",
                            self.vfuncvfc.name)
                    else:
                        self.__logger.info(
                            "Create VF %s",
                            self.vfuncvfc.name)
                        self.__logger.debug(
                            "VF using VFC name %s",
                            self.vfc_list[idx].name)
                        self.__logger.debug(
                            "VF using VFC unique_id %s",
                            self.vfc_list[idx].unique_id)
                        self.vfuncvfc.create_vfvfc(self.vfc_list[idx])
                        self.vfuncvfc.posx = 600 + 100 * idx
                        self.vfuncvfc.posy = 200 + 100 * idx
                        self.__logger.info(
                            "Check and update VF with VFC %s",
                            self.vfuncvfc.name)
                        # 2) Add VFC component to VF
                        self.vfuncvfc.addvfcvf(self.vfc_list[idx])
                        self.__logger.info(
                            "Add VFC component to VF %s", self.vfuncvfc.name)
                        # 3) Declare properties assignments
                        self.vfuncvfc.declarepropertiesvf(self.vfc_list[idx])
                        # 4) Modify inputs
                        # as added in vfname_vfcproperties.yaml
                        self.vfuncvfc.modifyinputs()

                    # 5) Checkin
                    if self.vfuncvfc.status in ["CERTIFIED",
                                                "CERTIFICATION_IN_PROGRESS",
                                                "READY_FOR_CERTIFICATION",
                                                "NOT_CERTIFIED_CHECKIN"]:
                        self.__logger.info("already done, skip checkin")
                    else:
                        self.__logger.info("vf checkin %s", self.vfuncvfc.name)
                        self.vfuncvfc.checkin_vfvfc()
                        self.__logger.info(
                            "Check and update VF %s", self.vfuncvfc.name)
                        self.vfuncvfc.check_vf_exists()

                    # 6) Submit
                    if self.vfuncvfc.status in ["CERTIFIED",
                                                "CERTIFICATION_IN_PROGRESS",
                                                "READY_FOR_TESTING"
                                                "READY_FOR_CERTIFICATION"]:
                        self.__logger.info("already done, skip submit")
                    else:
                        self.__logger.info("vf submit")
                        self.vfuncvfc.submit_for_testing_vfvfc()
                        self.__logger.info("Check and update VF")
                        self.vfuncvfc.check_vf_exists()

                    # 7) Start Certification
                    if self.vfuncvfc.status in ["CERTIFIED",
                                                "CERTIFICATION_IN_PROGRESS"]:
                        self.__logger.info(
                            "already done, skip Start Certification")
                    else:
                        self.__logger.info("vf Start Certification")
                        self.vfuncvfc.start_certif_vfvfc()
                        self.__logger.info("Check and update VF")
                        self.vfuncvfc.check_vf_exists()

                    # 8) Certify
                    if self.vfuncvfc.status == "CERTIFIED":
                        self.__logger.info("already done, skip Certify")
                    else:
                        self.__logger.info("vf certify")
                        self.vfuncvfc.certify_vfvfc()
                        self.__logger.info("Check and update VF")
                        self.vfuncvfc.check_vf_exists()
            else:
                self.__logger.info(
                    "vfvfc already Certified : skipping operations")

            # 9) update vf unique_id
            self.__logger.info("vf unique_id update")
            self.vfuncvfc.get_vf_metadata()

            self.vfuncvfc_list.append(copy.copy(self.vfuncvfc))

    def check_service_prerequisites(self):
        """
        Check service pre-requisites.

          * VF must exists
          * VF information must be loaded
        """
        onboard_prerequisites = True
        vf_found = False
        vl_found = False
        appendvfunc = False
        appendvfuncvfc = False
        service_params = onap_utils.get_service_custom_config(
            self.service.name)
        if self.vfunc_list == []:
            appendvfunc = True
        if self.vfuncvfc_list == []:
            appendvfuncvfc = True
        if "networks" in service_params:
            for net in service_params["networks"]:
                self.__logger.info("Check and load VL Info")
                self.vl.net_name = net["network_name"]
                self.vl.vl_name = net["vl_name"]
                if self.vl.check_vl_exists():
                    self.__logger.info("Load VL info")
                    vl_found = True
                    self.vl_list.append(copy.copy(self.vl))
                else:
                    self.__logger.info(
                        "VL %s does not exists : stop Service onboarding",
                        self.vl.vl_name)
        else:
            self.__logger.info(
                "No VLs in Service Description")
            vl_found = True

        if "vnfs" in service_params:
            for vnf in service_params["vnfs"]:
                self.vfunc.name = vnf["vnf_name"] + "_VF"
                if self.vfunc.check_vf_exists():
                    self.__logger.info("Check and load VF info")
                    vf_found = True
                    if appendvfunc:
                        self.vfunc_list.append(copy.copy(self.vfunc))
                else:
                    self.__logger.info(
                        "VF %s does not exists : stop Service onboarding",
                        self.vfunc.name)
                    onboard_prerequisites = False
        else:
            self.__logger.info(
                "No VFs in Service Description")
            vf_found = True
        if "vnfcs" in service_params:
            for vnf in service_params["vnfcs"]:
                self.vfuncvfc.name = vnf["vnf_name"] + "_VFC_VF"
                if self.vfuncvfc.check_vf_exists():
                    self.__logger.info("Check and load VFvfc info")
                    vf_found = True
                    if appendvfuncvfc:
                        self.vfuncvfc_list.append(copy.copy(self.vfuncvfc))
                else:
                    self.__logger.info(
                        "VFvfc %s not found: stop Service onboarding",
                        self.vfuncvfc.name)
                    onboard_prerequisites = False

        return onboard_prerequisites and vf_found and vl_found

    def create_service(self):
        """Create service."""
        if self.service.check_service_exists():
            self.__logger.info(
                "%s Service already exists: skip creation",
                self.service.name)
        else:
            self.__logger.info("Create Service")
            self.service.create_service()
            self.__logger.info("Check and update Service")
            self.service.check_service_exists()

    def checkin_service(self):
        """Checkin the service."""
        if self.service.status in ["CERTIFIED",
                                   "CERTIFICATION_IN_PROGRESS",
                                   "READY_FOR_CERTIFICATION",
                                   "NOT_CERTIFIED_CHECKIN"]:
            self.__logger.info("already done, skip checkin")
        else:
            self.__logger.info("service checkin")
            self.service.checkin_service()
            self.__logger.info("Check and update service")
            self.service.check_service_exists()

    def submit_service(self):
        """Submit the service."""
        self.__logger.debug("Test certification deprecated in Frankfurt")
        # if self.service.status in ["CERTIFIED",
        #                            "CERTIFICATION_IN_PROGRESS",
        #                            "READY_FOR_CERTIFICATION"]:
        #     self.__logger.info("already done, skip submit")
        # else:
        #     self.__logger.info("service submit for test")
        #     self.service.submit_for_testing_service()
        #     self.__logger.info("Check and update service")
        #     self.service.check_service_exists()

    def start_service_certification(self):
        """Start the service certification."""
        self.__logger.debug("Test certification deprecated in Frankfurt")
        # if self.service.status in ["CERTIFIED",
        #                            "CERTIFICATION_IN_PROGRESS"]:
        #     self.__logger.info("already done, skip Start Certification")
        # else:
        #     self.__logger.info("service Start Certification")
        #     self.service.start_certif_service()
        #     self.__logger.info("Check and update service")
        #     self.service.check_service_exists()

    def certify_service(self):
        """Certify the service."""
        if self.service.status == "CERTIFIED":
            self.__logger.info("already done, skip Certify")
        else:
            self.__logger.info("service certify")
            self.service.certify_service()
            self.__logger.info("Check and update service")
            self.service.check_service_exists()

    def approve_service(self):
        """.Approve the service."""
        if self.service.distri_status in ["DISTRIBUTION_APPROVED",
                                          "DISTRIBUTED"]:
            self.__logger.info("already done, skip Approval")
        else:
            self.__logger.info("service Start Certification")
            self.service.approve_service()
            self.__logger.info("Check and update service")
            self.service.check_service_exists()

    def distribute_service(self):
        """Distribute the service."""
        if self.service.distri_status == "DISTRIBUTED":
            self.__logger.info("already done, skip Distribution")
        else:
            self.__logger.info("service Distribution")
            self.service.distribute_service()
            self.__logger.info("Check and update service")
            self.service.check_service_exists()

    def post_onboard(self):
        """Post onboard."""
        self.initonapdata.declare_platform()
        self.initonapdata.declare_line_of_business()
        self.initonapdata.declare_project()
        self.initonapdata.declare_owningentity()
        if not self.service.check_service_exists_in_aai():
            self.__logger.info("declare_service_in_aai")
            self.service.declare_service_in_aai()
            self.service.check_service_exists_in_aai()
            self.__logger.info(
                "resource_version = %s",
                self.service.resource_version)
        if not self.initonapdata.check_customer_exists():
            self.__logger.info(
                "declare_customer = %s",
                self.initonapdata.customer_name)
            self.initonapdata.declare_customer()
        if not self.initonapdata.check_complex_exists():
            self.__logger.info("declare_complex")
            self.initonapdata.declare_complex()
            self.initonapdata.check_complex_exists()
        if not self.initonapdata.check_cloud_exists():
            self.__logger.info("declare_cloud_region")
            self.initonapdata.declare_cloud_region()
            self.initonapdata.check_cloud_exists()
            self.initonapdata.link_complex_to_cloud()
        if onap_utils.get_config("general.multicloud"):
            self.initonapdata.register_cloud_to_multicloud()
            time.sleep(10)
        if not self.initonapdata.check_tenant_exists():
            self.__logger.info("add_tenant_to_cloud")
            self.initonapdata.add_tenant_to_cloud()
            self.initonapdata.check_tenant_exists()
        self.service.add_relations()

    def onboard_service(self):
        """Onboard Service."""
        if self.check_service_prerequisites():
            self.service.check_service_exists()
            self.__logger.debug("Onboard service status %s",
                                self.service.status)
            if self.service.status != "DISTRIBUTED":
                # 1) Service creation
                self.create_service()

                # 2a) Add VL to Service
                i = 0
                for elem in self.vl_list:
                    self.__logger.info("Add VL %s to service",
                                       elem.net_name)
                    self.service.add_vl_to_service(elem)
#                    self.service.create_relation(elem.name, i, 'networks')
                    i += 1

                # 2b) Add VF (from VSP) to Service
                i = 0
                for elem in self.vfunc_list:
                    self.__logger.info("Add VF from VSP %s to service",
                                       elem.name)
                    self.service.add_vf_to_service(elem)
                    self.service.create_relation(elem.name, i, 'vnfs')
                    i += 1

                # 2c) Add VF (from VFC) to Service and create relations
                if self.vfuncvfc_list:
                    i = 0
                    for elem in self.vfuncvfc_list:
                        self.__logger.info("Add VF from VFC %s to service",
                                           elem.name)
                        # 2c1) Add VF (from VFC) to service
                        self.service.add_vf_to_service(elem)
                        # 2c2) Create relations between VFVFCs and VFs
                        self.service.create_relation(elem.name, i, 'vnfcs')
                        i += 1

                # 3) Checkin
                self.checkin_service()

                # 4) Submit
                self.submit_service()

                # 5) Start Certification
                self.start_service_certification()

                # 6) Certify
                self.certify_service()

                # 7) update service unique_id
                self.__logger.info("service unique_id update")
                self.service.get_service_metadata()

                # 8) Approve
                # self.approve_service() # Deprecated in Frankfurt

                # 9) Distribute
                self.distribute_service()

                # 10) Check distribution is completed
                distribution_id = self.service.get_distribution_id(
                    self.service.id)
                self.__logger.debug("Service uuid: %s", self.service.id)
                self.__logger.debug("Service distribution ID: %s",
                                    distribution_id)
                if (distribution_id and
                        self.service.is_service_distributed(distribution_id)):
                    self.__logger.info("Service distributed")
                else:
                    self.__logger.error("Service Not properly distributed")

                # 11) Dowload Tosca service template
                self.service.download_tosca_service()
            else:
                self.__logger.info("service already DISTRIBUTED")

            # 10) Post Onboarding in AAI
            self.post_onboard()
