#!/usr/bin/env python
# coding: utf-8
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
#  pylint: disable=too-many-instance-attributes
#  pylint: disable=too-many-branches
#  pylint: disable=too-many-public-methods
#  pylint: disable=too-many-arguments
#  pylint: disable=no-self-use
#  pylint: disable=too-many-locals
#  pylint: disable=too-many-statements
"""Service class used for onboarding."""

import datetime
import os
import logging
import json
import zipfile
import time
import requests
import urllib3

import onap_tests.utils.sender
import onap_tests.utils.utils as onap_utils
import onap_tests.utils.exceptions as onap_test_exceptions
import onap_tests.components.servrelation as servrelation

PROXY = onap_utils.get_config("general.proxy")
SDC_URL = onap_utils.get_config("onap.sdc.url")
SDC_URL2 = onap_utils.get_config("onap.sdc.url2")
SDC_HEADERS = onap_utils.get_config("onap.sdc.headers")
SDC_HEADERS_DESIGNER = SDC_HEADERS.copy()
SDC_HEADERS_DESIGNER["USER_ID"] = onap_utils.get_config("onap.sdc.designer_id")
SDC_HEADERS_TESTER = SDC_HEADERS.copy()
SDC_HEADERS_TESTER["USER_ID"] = onap_utils.get_config("onap.sdc.tester_id")
SDC_HEADERS_GOV = SDC_HEADERS.copy()
SDC_HEADERS_GOV["USER_ID"] = onap_utils.get_config("onap.sdc.governor_id")
SDC_HEADERS_OP = SDC_HEADERS.copy()
SDC_HEADERS_OP["USER_ID"] = onap_utils.get_config("onap.sdc.operator_id")
SDC_UPLOAD_HEADERS = onap_utils.get_config("onap.sdc.download_headers")
AAI_URL = onap_utils.get_config("onap.aai.url")
AAI_HEADERS = onap_utils.get_config("onap.aai.headers")

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)


class Service():
    """ONAP Service Object used for SDC operations."""

    __logger = logging.getLogger(__name__)

    def __init__(self, **kwargs):
        """Init Service class."""
        # pylint: disable=invalid-name
        self.id = ""
        self.inv_uuid = ""
        self.unique_id = ""
        self.resource_version = ""
        if "service_name" in kwargs:
            self.name = kwargs['service_name']
        else:
            self.name = "ONAP-test-service"

        self.version = ""
        self.status = ""
        self.distri_status = ""

        if "customer_name" in kwargs:
            self.customer_name = kwargs['customer_name']
        else:
            self.customer_name = onap_utils.get_config("onap.customer")
        self.cloud_owner = onap_utils.get_config("openstack.cloud_owner")
        self.cloud_region_id = onap_utils.get_config("openstack.region_id")
        self.tenant_id = onap_utils.get_config("openstack.tenant_id")
        self.tenant_name = onap_utils.get_config("openstack.tenant_name")
        self.servrelation = servrelation.Servrelation()
        self.__sender = onap_tests.utils.sender.Sender()

    def __send_message(self, method, action, url, header, **kwargs):
        trans_id = onap_utils.get_uuid()
        header.update(
            {"X-TransactionId": trans_id})
        return self.__sender.send_message('SDC', method, action,
                                          url, header, **kwargs)

    def __send_message_aai(self, method, action, url, header, **kwargs):
        return self.__sender.send_message('AAI', method, action,
                                          url, header, **kwargs)

    def __send_message_json(self, method, action, url, header, **kwargs):
        trans_id = onap_utils.get_uuid()
        header.update(
            {"X-TransactionId": trans_id})
        return self.__sender.send_message_json('SDC', method, action,
                                               url, header, **kwargs)

    def __send_message_json_aai(self, method, action, url, header, **kwargs):
        return self.__sender.send_message_json('AAI', method, action,
                                               url, header, **kwargs)

    def update_service(self, **kwargs):
        """Update Service values."""
        if "id" in kwargs:
            self.id = kwargs['id']
        if "inv_uuid" in kwargs:
            self.inv_uuid = kwargs['inv_uuid']
        if "unique_id" in kwargs:
            self.unique_id = kwargs['unique_id']
        if "resource_version" in kwargs:
            self.resource_version = kwargs['resource_version']
        if "name" in kwargs:
            self.name = kwargs['name']
        if "version" in kwargs:
            self.version = kwargs['version']
        if "status" in kwargs:
            self.status = kwargs['status']

        if "distri_status" in kwargs:
            self.distri_status = kwargs['distri_status']
        if "customer_name" in kwargs:
            self.customer_name = kwargs['customer_name']

    def get_sdc_service_payload(self, **kwargs):
        """Build SDC service payload."""
        try:
            payload = {}
            if kwargs['action'] == "Create":
                payload = onap_utils.get_config(
                    "service_payload.service_create_data")
                if "service_name" in kwargs:
                    payload['tags'][0] = kwargs['service_name']
                    payload['description'] = kwargs['service_name']
                    payload['name'] = kwargs['service_name']
                else:
                    payload['tags'][0] = "ONAP-test-service"
                    payload['description'] = "ONAP-test-service"
                    payload['name'] = "ONAP-test-service"
            if kwargs['action'] == "Add_vf_to_service":
                payload = onap_utils.get_config(
                    "service_payload.service_add_vf_data")
                if "vf_name" and "vf_unique_id" in kwargs:
                    payload['name'] = kwargs['vf_name']
                    dtime = datetime.datetime.now()
                    payload['uniqueId'] = (kwargs['vf_unique_id'] +
                                           dtime.strftime("%s"))
                    payload['componentUid'] = kwargs['vf_unique_id']
                    payload['posX'] = kwargs['posx']
                    payload['posY'] = kwargs['posy']
                # To be modified once version will be managed by all functions
                if "vf_version" in kwargs:
                    payload['componentVersion'] = kwargs['vf_version']
            if kwargs['action'] == "Modify_vl_in_service":
                payload = onap_utils.get_config(
                    "service_payload.service_modify_vl_data")
                payload['uniqueId'] = kwargs['vl_network_role_uniqueid']
                payload['parentUniqueId'] = (
                    kwargs['vl_network_role_parent_uniqueid'])
                payload['value'] = kwargs['vl_net_name']
                payload['ownerId'] = kwargs['vl_network_role_ownerid']
                # To be modified once version will be managed by all functions
                if "vl_version" in kwargs:
                    payload['componentVersion'] = kwargs['vl_version']
            if kwargs['action'] == "Add_vl_to_service":
                payload = onap_utils.get_config(
                    "service_payload.service_add_vl_data")
                if "vl_name" and "vl_unique_id" in kwargs:
                    payload['name'] = kwargs['vl_name']
                    dtime = datetime.datetime.now()
                    payload['uniqueId'] = (kwargs['vl_unique_id'] +
                                           dtime.strftime("%s"))
                    payload['componentUid'] = kwargs['vl_unique_id']
                    payload['posX'] = kwargs['posx']
                    payload['posY'] = kwargs['posy']
                    payload['originType'] = "VL"
                # To be modified once version will be managed by all functions
                if "vl_version" in kwargs:
                    payload['componentVersion'] = kwargs['vl_version']
            if kwargs['action'] == "Checkin":
                payload = onap_utils.get_config(
                    "service_payload.service_checkin_data")
            if kwargs['action'] == "Submit_for_testing":
                payload = onap_utils.get_config(
                    "service_payload.service_submit_for_testing_data")
            if kwargs['action'] == "Start_certification":
                payload = onap_utils.get_config(
                    "service_payload.service_start_certif_data")
            if kwargs['action'] == "Certify":
                payload = onap_utils.get_config(
                    "service_payload.service_certify_data")
            if kwargs['action'] == "Approve":
                payload = onap_utils.get_config(
                    "service_payload.service_approve_data")
            if kwargs['action'] == "Distribute":
                payload = onap_utils.get_config(
                    "service_payload.service_distribute_data")

        except KeyError:
            self.__logger.error("No payload set")
        return payload

    def get_aai_service_payload(self, **kwargs):
        """Build AAI service payload."""
        try:
            payload = {}
            if kwargs['action'] == "declare_service_in_aai":
                payload = onap_utils.get_config(
                    "service_payload.declare_service_in_aai_data")
                if "service_name" in kwargs:
                    payload['service-id'] = kwargs['service_invUUID']
                    payload['service-description'] = kwargs['service_name']
                else:
                    payload['service-id'] = kwargs['service_invUUID']
                    payload['service-description'] = "ONAP-test-service"

            if kwargs['action'] == "add_relations":
                payload = onap_utils.get_config(
                    "service_payload.add_relations_data")
                payload['service-type'] = kwargs['service_name']
                relationship_datas = payload['relationship-list']['relation\
ship'][0]["relationship-data"]
                for relationship_data in relationship_datas:
                    if relationship_data['relationship-key'] == "cloud-region\
.cloud-region-id":
                        relationship_data['relationship\
-value'] = self.cloud_region_id
                    if relationship_data['relationship\
-key'] == "tenant.tenant-id":
                        relationship_data['relationship\
-value'] = self.tenant_id
                    if relationship_data['relationship\
-key'] == "cloud-region.cloud-owner":
                        relationship_data['relationship\
-value'] = self.cloud_owner
                related_to_property = payload['relationship\
-list']['relationship'][0]["related-to-property"]
                related_to_property[0]['property-value'] = self.tenant_name
                related_link = str(payload['relationship\
-list']['relationship'][0]["related-link"])
                related_link = related_link.replace(
                    "PPcloud_region_idPP", self.cloud_region_id)
                related_link = related_link.replace(
                    "PPtenant_idPP", self.tenant_id)
                related_link = related_link.replace(
                    "PPcloud_ownerPP", self.cloud_owner)
                payload['relationship-list']['relationship'][0]["related\
-link"] = related_link

        except KeyError:
            self.__logger.error("No payload set")
        return payload

    def get_service_list(self):
        """
        Get service list.

        :return: the list of the services
        """
        url = SDC_URL + onap_utils.get_config("onap.sdc.list_service_url")
        headers = SDC_HEADERS_DESIGNER
        return self.__send_message_json('GET', 'get services',
                                        url, headers)

    def get_service_list_in_aai(self):
        """Get service list in AAI."""
        url = AAI_URL + onap_utils.get_config("onap.aai.list_service_url")
        headers = AAI_HEADERS
        return self.__send_message_json_aai('GET', 'get services in AAI',
                                            url, headers)

    def get_service_metadata(self):
        """Get service metadata to get service unique_id when new version."""
        service_unique_id_updated = False
        old_str = onap_utils.get_config("onap.sdc.metadata_service_url")
        new_str = old_str.replace("PPservice_unique_idPP", self.unique_id)
        url = SDC_URL2 + new_str
        headers = SDC_HEADERS_DESIGNER
        response = self.__send_message_json('GET', 'get service metadata',
                                            url, headers)
        if response:
            service_unique_id_updated = True
            # only working with 1.0 version
            self.update_service(
                unique_id=response["metadata"]["allVersions"]["1.0"])
            self.__logger.info(
                "[SDC][Get service metadata] Service unique Id : %s",
                self.unique_id)
        else:
            self.__logger.error(
                "Get service metadata code : %s", response.status_code)
            self.__logger.error(
                "Service unique Id : %s", self.unique_id)

        return service_unique_id_updated

    def check_service_exists(self):
        """Check if provided service exists in service list."""
        service_list = self.get_service_list()
        service_found = False
        if service_list:
            for result in service_list:
                if result['name'] == self.name:
                    service_found = True
                    self.update_service(id=result["uuid"])
                    self.update_service(version=result["version"])
                    self.update_service(status=result["lifecycleState"])
                    self.update_service(
                        distri_status=result["distributionStatus"])
                    self.update_service(inv_uuid=result["invariantUUID"])
        urlpart = onap_utils.get_config(
            "onap.sdc.get_catalog_resource_list_url")

        url = SDC_URL2 + urlpart
        headers = SDC_HEADERS_DESIGNER
        response = self.__send_message_json('GET', 'get resources',
                                            url, headers)
        if response:
            for resource in response["services"]:
                if resource["uuid"] == self.id:
                    self.__logger.debug(
                        "Service resource %s found in service list",
                        resource["name"])
                    self.update_service(unique_id=resource["uniqueId"])

        self.__logger.debug("service found parameter %s", service_found)
        return service_found

    def check_service_exists_in_aai(self):
        """Check if provided service exists in AAI service list."""
        self.__logger.info("check if %s exists in AAI", self.name)
        service_list = self.get_service_list_in_aai()
        service_found_in_aai = False
        if service_list:
            for result in service_list['service']:
                if result['service-description'] == self.name:
                    service_found_in_aai = True
                    self.update_service(
                        resource_version=result["resource-version"])
        return service_found_in_aai

    def create_service(self):
        """Create service in SDC (only if it does not exist)."""
        # we check if service exists or not, if not we create it
        create_service = False
        if not self.check_service_exists():
            url = SDC_URL2 + onap_utils.get_config(
                "onap.sdc.create_service_url")
            data = self.get_sdc_service_payload(action="Create",
                                                service_name=self.name)
            data = json.dumps(data)
            headers = SDC_HEADERS_DESIGNER
            response = self.__send_message_json('POST', 'Create service',
                                                url, headers, data=data)
            if response:
                create_service = True

                self.update_service(id=response["uuid"])
                self.update_service(unique_id=response["uniqueId"])
                self.__logger.info(
                    "[SDC][Create service] Service unique Id : %s",
                    self.unique_id)
            else:
                self.__logger.error(
                    "Create service code : %s", response.status_code)
                self.__logger.error(
                    "Service unique Id : %s", self.unique_id)

        return create_service

    def add_vf_to_service(self, vfunc):
        """
        Add a Virtual Function (VF) to service in SDC.

        :param vfunc: the VF to be added to the service
        """
        state = True
        if self.status == "NOT_CERTIFIED_CHECKOUT":
            old_str = onap_utils.get_config("onap.sdc.add_vf_to_service_url")
            new_str = old_str.replace("PPservice_uuidPP", self.unique_id)
            url = SDC_URL2 + new_str
            data = self.get_sdc_service_payload(action="Add_vf_to_service",
                                                vf_name=vfunc.name,
                                                vf_unique_id=vfunc.unique_id,
                                                vf_version=vfunc.version,
                                                posx=vfunc.posx,
                                                posy=vfunc.posy)
            data = json.dumps(data)
            headers = SDC_HEADERS_DESIGNER
            state = bool(self.__send_message('POST', 'Add VF to service',
                                             url, headers, data=data))
        else:
            self.__logger.info(
                "Service: %s is not checked out, skip adding VF %s",
                self.name, vfunc.name)
        return state

    def add_vl_to_service(self, vlink):
        """
        Add Virtual link (VL) to service in SDC.

        :param vlik: the Virtual link to be added to the service
        """
        if self.status == "NOT_CERTIFIED_CHECKOUT":
            found_vl = False
            url = SDC_URL2 + onap_utils.get_config(
                "onap.sdc.get_services_instance_url").\
                replace("PPservice_uuidPP", self.unique_id)
            headers = SDC_HEADERS_DESIGNER
            response = self.__send_message_json('GET',
                                                'get service instance',
                                                url, headers)
            if 'componentInstances' in response:
                for result in response['componentInstances']:
                    if result['name'] == vlink.net_name:
                        found_vl = True
                        self.__logger.info(
                            "Found VL in service: %s", vlink.net_name)
                        break

            config_vl = False

            if not found_vl:
                old_str = onap_utils.get_config(
                    "onap.sdc.add_vl_to_service_url")
                new_str = old_str.replace("PPservice_uuidPP", self.unique_id)
                url = SDC_URL2 + new_str
                data = self.get_sdc_service_payload(
                    action="Add_vl_to_service", vl_name=vlink.net_name,
                    vl_unique_id=vlink.unique_id, vl_version=vlink.version,
                    posx=vlink.posx, posy=vlink.posy)
                data = json.dumps(data)
                headers = SDC_HEADERS_DESIGNER
                found_vl = bool(self.__send_message('POST',
                                                    'Add VL to service',
                                                    url, headers, data=data))
            if found_vl:
                config_vl = self.configure_vl(vlink.net_name)
            else:
                self.__logger.error(
                    "Failed to add VL to service: %s", response)
                return found_vl
            return config_vl
        return True

    def configure_vl(self, vl_net_name):
        """
        Configure VL network_role attribute.

        :param vl_net_name: the virtual link net name
        """
        found_component = False
        found_network_role = False
        vl_instance_unique_id = ""
        vl_network_role_uniqueid = ""
        vl_network_role_parent_uniqueid = ""
        vl_network_role_ownerid = ""
        url = SDC_URL2 + onap_utils.get_config(
            "onap.sdc.get_services_instance_url").\
            replace("PPservice_uuidPP", self.unique_id)
        headers = SDC_HEADERS_DESIGNER
        response = self.__send_message_json('GET',
                                            'get service instance',
                                            url, headers)
        for result in response['componentInstances']:
            if result['name'] == vl_net_name:
                found_component = True
                vl_instance_unique_id = result['uniqueId']
                self.__logger.info("Found VL component: %s", vl_net_name)
                break
        if not found_component:
            self.__logger.error(
                "Failed to find VL component: %s", vl_net_name)
            return False

        res = response['componentInstancesProperties'][vl_instance_unique_id]
        for result in res:
            if result['name'] == "network_role":
                found_network_role = True
                vl_network_role_uniqueid = result['uniqueId']
                vl_network_role_parent_uniqueid = result['parentUniqueId']
                vl_network_role_ownerid = result['ownerId']
                break
        if not found_network_role:
            self.__logger.error(
                "Failed to find VL network role: %s", vl_net_name)
            return False

        url = (SDC_URL2 +
               onap_utils.get_config(
                   "onap.sdc.update_vl_instance_property_url"))
        url = url.replace("PPservice_uuidPP", self.unique_id)
        url = url.replace("PPvl_unique_idPP", vl_instance_unique_id)
        data = self.get_sdc_service_payload(
            action="Modify_vl_in_service",
            vl_network_role_uniqueid=vl_network_role_uniqueid,
            vl_network_role_parent_uniqueid=vl_network_role_parent_uniqueid,
            vl_net_name=vl_net_name,
            vl_network_role_ownerid=vl_network_role_ownerid)
        data = '[' + json.dumps(data) + ']'
        headers = SDC_HEADERS_DESIGNER
        return bool(self.__send_message('POST', 'Modify VL in service',
                                        url, headers, data=data))

    def get_component_instances(self, vf_name, vflink_name, relation_type):
        """
        Get inputs from get service componentInstances.

        :param vf_name: the name of the VF
        :param vflink_name: the name of the VL
        :param relation_type: the type of the relation
        :return: the instance components
        """
        get_component_instances = ""
        url = SDC_URL2 + onap_utils.get_config(
            "onap.sdc.get_services_componentInstances_url").\
            replace("PPservice_uuidPP", self.unique_id)
        headers = SDC_HEADERS_DESIGNER
        response = self.__send_message_json('GET',
                                            'get services component instances',
                                            url, headers)
        payload = self.servrelation.set_payload_relation(response,
                                                         vf_name,
                                                         vflink_name,
                                                         relation_type)
        try:
            get_component_instances = json.dumps(payload)
        except TypeError as err:
            self.__logger.error(
                "Failed to get service instance components: %s", err)
        return get_component_instances

    def create_relation(self, vf_name, idx, typevf):
        """
        Create config relation with VFs.

        :param vf_name: the name of the VF
        :param idx: the index of the VF
        :param typevf: the type of the VF
        :return: True if relation successfully create else False
        """
        # Get the list of relations to create
        service_params = onap_utils.get_service_custom_config(
            self.name)
        params = service_params[typevf][idx]
        if 'relationtype' in params and 'relationwithvnf' in params:
            relationtype = service_params[typevf][idx]['relationtype']
            self.__logger.debug(
                "[SDC][Create relations between VFs] relationtype: %s",
                relationtype)
            for index in service_params[typevf][idx]['relationwithvnf']:
                vflink_name = service_params['vnfs'][index]['vnf_name'] + "_VF"
                self.__logger.debug(
                    "[SDC][Create relations between VFs] Index value: %s",
                    idx)
                self.__logger.info(
                    "[Create relations between VFs] from VF: %s", vf_name)
                self.__logger.debug(
                    "[SDC][Create relations between VFs] to VF: %s",
                    vflink_name)
                old_str = onap_utils.get_config(
                    "onap.sdc.create_relation_between_vf_url")
                url = SDC_URL2 + old_str.replace(
                    "PPservice_uuidPP",
                    self.unique_id)
                data = self.get_component_instances(vf_name,
                                                    vflink_name,
                                                    relationtype)
                return bool(self.__send_message_json(
                    'POST',
                    'Create relations between VFs',
                    url, SDC_HEADERS_DESIGNER, data=data))
        return False

    def upgrade_vf_to_service(self, vfunc):
        """
        Upgrade VF to service in SDC.

        :param vfunc: the virtual function
        :return: True if VF updated successfully else False
        """
        old_str = onap_utils.get_config("onap.sdc.upgrade_vf_to_service_url")
        new_str = old_str.replace("PPservice_uuidPP", self.unique_id)
        url = SDC_URL2 + new_str
        data = self.get_sdc_service_payload(action="Upgrade_vf_to_service",
                                            vf_name=vfunc.name,
                                            vf_unique_id=vfunc.unique_id,
                                            posx=vfunc.posx)
        data = json.dumps(data)
        headers = SDC_HEADERS_DESIGNER
        return bool(self.__send_message_json(
            'POST',
            'Upgrade VF to Service',
            url, headers, data=data))

    def get_service_info(self, service):
        """
        Get service detail.

        :param service: the service
        :return: the service details
        """
        service_list = self.get_service_list()
        for result in service_list:
            if result['name'] == service['name']:
                service = result
                break
        return service

    def __action_on_service(self, config_path, action, headers, start_url,
                            input_id):
        """
        Trigger an action on Service (checkin, checkout, submit for testing,.).

        :param config_path: the path of the configuration file
        :param action: the expected action on the service
        :param headers:: the headers
        :param start_url: the SDC url
        :param input_id: the value of the Service UUID to put in the config
        :return: True if action successfull else False
        """
        old_str = onap_utils.get_config(config_path)
        new_str = old_str.replace("PPservice_uuidPP", input_id)
        url = start_url + new_str
        data = self.get_sdc_service_payload(action=action)
        data = json.dumps(data)
        return bool(self.__send_message('POST', "{} service".format(action),
                                        url, headers, data=data))

    def __action_on_service_be(self, config_path, action, headers):
        return self.__action_on_service(config_path, action, headers,
                                        SDC_URL, self.id)

    def __action_on_service_fe(self, config_path, action, headers):
        return self.__action_on_service(config_path, action, headers,
                                        SDC_URL2, self.unique_id)

    def checkin_service(self):
        """Checkin service."""
        return self.__action_on_service_be("onap.sdc.checkin_service_url",
                                           "Checkin", SDC_HEADERS_DESIGNER)

    def checkout_service(self):
        """Checkout service."""
        checkout_service = False
        old_str = onap_utils.get_config("onap.sdc.checkout_service_url")
        new_str = old_str.replace("PPservice_uuidPP", self.unique_id)
        url = SDC_URL2 + new_str
        data = self.get_sdc_service_payload(action="Checkout")
        data = json.dumps(data)
        headers = SDC_HEADERS_DESIGNER
        response = self.__send_message_json('POST', "Checkout service",
                                            url, headers, data=data)
        if response:
            checkout_service = True
            self.update_service(
                unique_id=response["uniqueId"])
            self.__logger.info(
                "[SDC][Checkout service] Service unique Id : %s",
                self.unique_id)
        return checkout_service

    def submit_for_testing_service(self):
        """Submit_for_testing service."""
        return self.__action_on_service_be(
            "onap.sdc.submit_for_testing_service_url", "Submit_for_testing",
            SDC_HEADERS_DESIGNER)

    def start_certif_service(self):
        """Start the certification of the service."""
        return self.__action_on_service_be(
            "onap.sdc.start_certif_service_url", "Start_certification",
            SDC_HEADERS_TESTER)

    def certify_service(self):
        """Certify the service."""
        return self.__action_on_service_be(
            "onap.sdc.certify_service_url", "Certify", SDC_HEADERS_DESIGNER)

    def approve_service(self):
        """Approve the service."""
        return self.__action_on_service_fe(
            "onap.sdc.approve_service_url", "Approve", SDC_HEADERS_DESIGNER)

    def distribute_service(self):
        """Distribute service."""
        return self.__action_on_service_fe("onap.sdc.distribute_service_url",
                                           "Distribute", SDC_HEADERS_DESIGNER)

    def download_tosca_service(self, **kwargs):
        """Download service CSAR file and save/store service template."""
        if "service_uuid" in kwargs:
            self.id = kwargs['service_uuid']
        if "service_name" in kwargs:
            self.name = kwargs['service_name']
        url = (SDC_URL +
               onap_utils.get_config(
                   "onap.sdc.download_tosca_service_url").replace(
                       "PPservice_uuidPP",
                       self.id))
        self.__logger.debug("download CSAR URL : %s", url)
        try:
            response = self.__send_message('GET', "Download Tosca service",
                                           url, header=SDC_UPLOAD_HEADERS)
            response.raise_for_status()
            self.__logger.info("[SDC][Download CSAR] response code: %s",
                               response.status_code)
            path = os.path.dirname(os.path.abspath(__file__)).replace(
                "components", "templates/tosca_files/")
            csar_filename = "service-" + self.name + "-csar.csar"
            self.__logger.debug(
                "[SDC][Download CSAR] CSAR filename: %s",
                (path + csar_filename))
            with open((path + csar_filename), 'wb') as csar_file:
                for chunk in response.iter_content(chunk_size=128):
                    csar_file.write(chunk)
            with zipfile.ZipFile(path + csar_filename) as myzip:
                for name in myzip.namelist():
                    if (name[-13:] == "-template.yml" and
                            name[:20] == "Definitions/service-"):
                        service_template = name
                with myzip.open(service_template) as file1:
                    with open(path + service_template[12:], 'wb') as file2:
                        file2.write(file1.read())
        except requests.HTTPError:
            self.__logger.error("[SDC][Download CSAR] response code: %s",
                                response.status_code)
            self.__logger.error("[SDC][Download CSAR] sent header: %s",
                                SDC_UPLOAD_HEADERS)
            self.__logger.error("[SDC][Download CSAR] url used: %s", url)
            self.__logger.error("[SDC][Download CSAR] response: %s",
                                response.text)
            raise onap_test_exceptions.CsarDownloadException
        except requests.RequestException as err:
            self.__logger.error("[SDC][Download CSAR]Failed to perform: %s",
                                err)
            self.__logger.error("[SDC][Download CSAR] sent header: %s",
                                SDC_UPLOAD_HEADERS)
            self.__logger.error("[SDC][Download CSAR] url used: %s", url)
            raise onap_test_exceptions.CsarDownloadException

    def declare_service_in_aai(self):
        """Declare the service in AAI (subscription notion)."""
        old_str = onap_utils.get_config(
            "onap.aai.declare_service_in_aai_url")
        new_str = old_str.replace(
            "PPservice_invUUIDPP", self.inv_uuid)
        url = AAI_URL + new_str
        data = self.get_aai_service_payload(
            action="declare_service_in_aai",
            service_name=self.name,
            service_invUUID=self.inv_uuid)
        data = json.dumps(data)
        headers = AAI_HEADERS
        return bool(self.__send_message_aai('PUT', "Declare Service",
                                            url, headers, data=data))

    def add_relations(self):
        """Add Customer, Service and Tenant relations."""
        old_str = onap_utils.get_config(
            "onap.aai.add_relations_url")
        new_str1 = old_str.replace("PPcustomer_namePP", self.customer_name)
        new_str2 = new_str1.replace(
            "PPservice_namePP", onap_utils.get_subscription_type(self.name))
        new_str3 = new_str2.replace(
            "PPservice_resource_versionPP", self.resource_version)
        url = AAI_URL + new_str3
        data = self.get_aai_service_payload(
            action="add_relations",
            service_name=onap_utils.get_subscription_type(self.name),
            cloud_region_id=self.cloud_region_id,
            tenant_id=self.tenant_id,
            tenant_name=self.tenant_name)
        data = json.dumps(data)
        headers = AAI_HEADERS
        return bool(self.__send_message_aai('PUT', "Add relations",
                                            url, headers, data=data))

    def get_sdc_headers(self, oldkey, newkey, user):
        """
        Get SDC headers.

        :param oldkey: the default old key
        :param newkey: the new key to be indicated in SDC headers
        :param user: the ONAP user
        :return: SDC headers
        """
        # get SDC header and replace the header by the one specified in param
        sdc_new_headers = SDC_HEADERS
        self.__logger.debug("old headers: %s", sdc_new_headers)
        if oldkey in sdc_new_headers:
            sdc_new_headers[newkey] = sdc_new_headers.pop(oldkey)
        sdc_new_headers[newkey] = user
        self.__logger.debug("header field modified %s.", sdc_new_headers)
        return sdc_new_headers

    def get_distribution_id(self, service_id):
        """
        Get distribution ID based on Service UUID.

        :param service_id: the id of the service
        :return: the distribution id
        """
        distribution_id = None
        url = (SDC_URL2 + "/sdc1/feProxy/rest/v1/catalog/services/" +
               service_id + "/distribution")
        response = self.__send_message_json('GET', "Get Distribution ID",
                                            url, SDC_HEADERS_DESIGNER)
        if response and response['distributionStatusOfServiceList']:
            dist_status = response['distributionStatusOfServiceList'][0]
            distribution_id = dist_status['distributionID']
        return distribution_id

    def is_service_distributed(self, distribution_id):
        """
        Check if the Service has been properly distributed in SO, AAI and SDNC.

        :param distribution_id: the distribution id
        :return: True if service successfully distributed in aai, sdnc and so
        """
        url = (SDC_URL2 +
               "/sdc1/feProxy/rest/v1/catalog/services/distribution/" +
               distribution_id)
        request_completed = False
        nb_try = 0
        nb_try_max = 10
        # headers = self.get_sdc_headers("USER_ID", "user_id", "op0001")
        components = ["SO", "sdnc", "aai"]
        status = {'aai': False, 'SO': False, 'sdnc': False}
        while request_completed is False and nb_try < nb_try_max:
            response = self.__send_message_json('GET', "Get Distribution",
                                                url, SDC_HEADERS_DESIGNER)
            self.__logger.info(
                "[SDC][Get Distribution] %s distribution status....",
                distribution_id)
            # self.__logger.debug("SDC answer: %s", response)
            if response:
                distrib_list = response['distributionStatusList']
                self.__logger.debug(
                    "[SDC][Get Distribution] distrib_list = %s", distrib_list)
                for elt in distrib_list:
                    for i in components:
                        if (i in elt['omfComponentID'] and
                                'DOWNLOAD_OK' in elt['status']):
                            status[i] = True
                            self.__logger.info(
                                "[SDC][Get Distribution] Service distributed \
                                in %s", i)
            if status['aai'] and status['SO'] and status['sdnc']:
                self.__logger.info("[SDC][Get Distribution] fully distributed")
                return True
            time.sleep(60)
            nb_try += 1

        if request_completed is False:
            self.__logger.info(
                "[SDC][Get Distribution] Distribution not seen completed")
            raise onap_test_exceptions.SdcRequestException
