#!/usr/bin/env python
# coding: utf-8
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
#  pylint: disable=missing-docstring
#  pylint: disable=too-many-instance-attributes
"""VF class used for onboarding."""
import logging
import json

import onap_tests.utils.sender
import onap_tests.utils.utils as onap_utils

PROXY = onap_utils.get_config("general.proxy")
SDC_URL = onap_utils.get_config("onap.sdc.url")
SDC_URL2 = onap_utils.get_config("onap.sdc.url2")
SDC_HEADERS = onap_utils.get_config("onap.sdc.headers")
SDC_HEADERS_DESIGNER = SDC_HEADERS.copy()
SDC_HEADERS_DESIGNER["USER_ID"] = "cs0008"
SDC_HEADERS_TESTER = SDC_HEADERS.copy()
SDC_HEADERS_TESTER["USER_ID"] = "jm0007"
SDC_UPLOAD_HEADERS = onap_utils.get_config("onap.sdc.upload_headers")


class VF():
    """ONAP VF Object used for SDC operations."""

    __logger = logging.getLogger(__name__)

    def __init__(self, **kwargs):
        """Init VF class."""
        # pylint: disable=invalid-name
        self.id = ""
        self.unique_id = ""
        if "vf_name" in kwargs:
            self.name = kwargs['vf_name']
        else:
            self.name = "ONAP-test-VF"
        self.version = ""
        self.status = ""
        self.posx = 200
        self.posy = 200
        self.__sender = onap_tests.utils.sender.Sender()

    def __send_message(self, method, action, url, header, **kwargs):
        trans_id = onap_utils.get_uuid()
        header.update(
            {"X-TransactionId": trans_id})
        return self.__sender.send_message('SDC', method, action,
                                          url, header, **kwargs)

    def __send_message_json(self, method, action, url, header, **kwargs):
        trans_id = onap_utils.get_uuid()
        header.update(
            {"X-TransactionId": trans_id})
        return self.__sender.send_message_json('SDC', method, action,
                                               url, header, **kwargs)

    def update_vf(self, **kwargs):
        """Update VF values."""
        if "id" in kwargs:
            self.id = kwargs['id']
        if "unique_id" in kwargs:
            self.unique_id = kwargs['unique_id']
        if "name" in kwargs:
            self.name = kwargs['name']
        if "version" in kwargs:
            self.version = kwargs['version']
        if "status" in kwargs:
            self.status = kwargs['status']

    def get_sdc_vf_payload(self, **kwargs):
        """Build SDC VF payload."""
        try:
            payload = {}
            if kwargs['action'] == "Create":
                payload = onap_utils.get_config(
                    "vf_payload.vf_create_data")
                if "vendor_name" and "vsp_name" and \
                   "vf_name" and "csar_uuid" in kwargs:
                    payload['vendorName'] = kwargs['vendor_name']
                    payload['tags'][0] = kwargs['vf_name']
                    payload['name'] = kwargs['vf_name']
                    payload['csarUUID'] = kwargs['csar_uuid']
            if kwargs['action'] == "Checkin":
                payload = onap_utils.get_config(
                    "vf_payload.vf_checkin_data")
            if kwargs['action'] == "Submit_for_testing":
                payload = onap_utils.get_config(
                    "vf_payload.vf_submit_for_testing_data")
            if kwargs['action'] == "Start_certification":
                payload = onap_utils.get_config(
                    "vf_payload.vf_start_certification_data")
            if kwargs['action'] == "Certify":
                payload = onap_utils.get_config(
                    "vf_payload.vf_certify_data")
        except KeyError:
            self.__logger.error("No payload set")
        return payload

    def get_vf_list(self):
        """Get vf list."""
        url = SDC_URL + onap_utils.get_config(
            "onap.sdc.list_vf_url")
        headers = SDC_HEADERS_DESIGNER
        return self.__send_message_json('GET', 'get vfs',
                                        url, headers)

    def get_vf_metadata(self):
        """Get vf metadata to get vf unique_id when version is changing."""
        vf_unique_id_updated = False
        old_str = onap_utils.get_config("onap.sdc.metadata_vf_url")
        new_str = old_str.replace("PPvf_unique_idPP", self.unique_id)
        url = SDC_URL2 + new_str
        headers = SDC_HEADERS_DESIGNER
        response = self.__send_message_json('GET', 'get vf metadata',
                                            url, headers)
        if response:
            vf_unique_id_updated = True
            self.update_vf(
                unique_id=response["metadata"]["allVersions"]["1.0"])
        self.__logger.info("[SDC][get vf metadata] VF unique Id : %s",
                           self.unique_id)
        return vf_unique_id_updated

    def check_vf_exists(self):
        """Check if provided vf exists in vf list."""
        vf_list = self.get_vf_list()
        vf_found = False
        for result in vf_list:
            if result['name'] == self.name:
                vf_found = True
                self.__logger.info("VF %s found in VF list", self.name)
                self.update_vf(id=result["uuid"])
                self.update_vf(version=result["version"])
                self.update_vf(status=result["lifecycleState"])
        if not vf_found:
            self.__logger.info("VF in not in vf list: %s", self.name)
            self.update_vf(id="")
            self.update_vf(unique_id="")
            self.update_vf(version="")
            self.update_vf(status="")

        if vf_found:
            # Get detailed content of created VF, especially unique_id
            urlpart = onap_utils.get_config(
                "onap.sdc.get_resource_list_url")
            url = SDC_URL2 + urlpart
            headers = SDC_HEADERS_DESIGNER
            response = self.__send_message_json('GET', 'get vf',
                                                url, headers)
            if response:
                for resource in response["resources"]:
                    if resource["uuid"] == self.id:
                        self.__logger.info(
                            "[SDC][get vf] VF resource %s found",
                            resource["name"])
                        self.__logger.debug(
                            "[SDC][get vf] VF resource %s uniqueId",
                            resource["uniqueId"])
                        self.update_vf(unique_id=resource["uniqueId"],
                                       vendor_name=resource["vendorName"])
            self.__logger.info("[SDC][get vf] VF found parameter %s", vf_found)

        return vf_found

    def create_vf(self, vsp):
        """
        Create vf in SDC (only if it does not exist).

        :param vsp; the CSP to be added to the VF
        """
        # we check if vf exists or not, if not we create it
        create_vf = False
        if not self.check_vf_exists():
            url = SDC_URL2 + onap_utils.get_config(
                "onap.sdc.create_vf_url")
            data = self.get_sdc_vf_payload(action="Create",
                                           vf_name=self.name,
                                           vendor_name=vsp.vendor_name,
                                           vsp_name=vsp.name,
                                           csar_uuid=vsp.csar_uuid)
            data = json.dumps(data)
            headers = SDC_HEADERS_DESIGNER
            response = self.__send_message_json('POST', 'Create vf',
                                                url, headers, data=data)
            if response:
                create_vf = True
                self.update_vf(id=response["uuid"])
                self.update_vf(unique_id=response["uniqueId"])
                self.__logger.info(
                    "[SDC][Create vf] VF Id : %s", self.id)
                self.__logger.info(
                    "[SDC][Create vf] VF unique Id : %s", self.unique_id)
        return create_vf

    def upgrade_vf(self, vsp):
        """
        Upgrade VF in SDC.

        :param vsp: the VSP to be upgraded in the VF
        """
        upgrade_vf = False
        url = SDC_URL2 + onap_utils.get_config(
            "onap.sdc.upgrade_vf_url")
        data = self.get_sdc_vf_payload(action="Upgrade",
                                       vf_name=self.name,
                                       vendor_name=vsp.vendor_name,
                                       vsp_name=vsp.name,
                                       csar_uuid=vsp.csar_uuid)
        data = json.dumps(data)
        headers = SDC_HEADERS_DESIGNER
        response = self.__send_message_json('POST', 'Upgrade vf',
                                            url, headers, data=data)
        if response:
            upgrade_vf = True
            self.update_vf(id=response["uuid"])
            self.update_vf(unique_id=response["uniqueId"])
            self.__logger.info(
                "[SDC][Upgrade vf] VF Id : %s", self.id)
            self.__logger.info(
                "[SDC][Upgrade vf] VF unique Id : %s", self.unique_id)
        return upgrade_vf

    def get_vf_info(self, vfunction):
        """
        Get VF requestDetails.

        :param vffunction: the VF name
        """
        vf_list = self.get_vf_list()
        for result in vf_list:
            if result['name'] == vfunction['name']:
                vfunction = result
                break
        return vfunction

    def __action_on_vf(self, config_path, action):
        """Trigger action on VF (checkin, checkout, certification)."""
        old_str = onap_utils.get_config(config_path)
        new_str = old_str.replace("PPvf_uuidPP", self.id)
        url = SDC_URL + new_str
        data = self.get_sdc_vf_payload(action=action)
        data = json.dumps(data)
        return bool(self.__send_message_json('POST', "{} vf".format(action),
                                             url, SDC_HEADERS_DESIGNER,
                                             data=data))

    def checkin_vf(self):
        """Checkin the VF."""
        return self.__action_on_vf("onap.sdc.checkin_vf_url", "Checkin")

    def checkout_vf(self):
        """Checkout the VF."""
        return self.__action_on_vf("onap.sdc.checkout_vf_url", "Checkout")

    def certify_vf(self):
        """Certify the VF."""
        return self.__action_on_vf("onap.sdc.certify_vf_url", "Certify")
