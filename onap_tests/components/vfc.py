#!/usr/bin/env python
# coding: utf-8
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
#  pylint: disable=missing-docstring
#  pylint: disable=too-many-instance-attributes
"""VFC class used for onboarding."""
import logging
import json
from copy import deepcopy
import base64
import os
import hashlib

import onap_tests.utils.exceptions as onap_test_exceptions
import onap_tests.utils.utils as onap_utils
import onap_tests.utils.sender

PROXY = onap_utils.get_config("general.proxy")
SDC_URL = onap_utils.get_config("onap.sdc.url")
SDC_URL2 = onap_utils.get_config("onap.sdc.url2")
SDC_HEADERS = onap_utils.get_config("onap.sdc.headers")
SDC_HEADERS_DESIGNER = SDC_HEADERS.copy()
SDC_HEADERS_DESIGNER["USER_ID"] = "cs0008"
SDC_HEADERS_TESTER = SDC_HEADERS.copy()
SDC_HEADERS_TESTER["USER_ID"] = "jm0007"
SDC_UPLOAD_HEADERS = onap_utils.get_config("onap.sdc.upload_headers")
SDC_UPLOAD_HEADERS_VFC = onap_utils.get_config("onap.sdc.vfc_upload_headers")


class VFC():
    """ONAP VFC Object used for SDC operations."""

    __logger = logging.getLogger(__name__)

    def __init__(self, **kwargs):
        """Init VFC class."""
        # pylint: disable=invalid-name
        self.__sender = onap_tests.utils.sender.Sender()
        self.id = ""
        if "vfc_name" in kwargs:
            self.name = kwargs['vfc_name']
        else:
            self.name = ""
        self.version = ""
        self.status = ""
        self.vendor_name = ""
        self.unique_id = ""
        if "service_name" in kwargs:
            self.service_name = kwargs['service_name']
        else:
            self.name = ""

    def __send_message(self, method, action, url, header, **kwargs):
        trans_id = onap_utils.get_uuid()
        header.update(
            {"X-TransactionId": trans_id})
        return self.__sender.send_message('SDC', method, action,
                                          url, header, **kwargs)

    def __send_message_json(self, method, action, url, header, **kwargs):
        trans_id = onap_utils.get_uuid()
        header.update(
            {"X-TransactionId": trans_id})
        return self.__sender.send_message_json('SDC', method, action,
                                               url, header, **kwargs)

    def update_vfc(self, **kwargs):
        """Update vendor values."""
        if "id" in kwargs:
            self.id = kwargs['id']
        if "unique_id" in kwargs:
            self.unique_id = kwargs['unique_id']
        if "name" in kwargs:
            self.name = kwargs['name']
        if "service_name" in kwargs:
            self.service_name = kwargs['service_name']
        if "version" in kwargs:
            self.version = kwargs['version']
        if "status" in kwargs:
            self.status = kwargs['status']
        if "vendor_name" in kwargs:
            self.vendor_name = kwargs['vendor_name']

    def get_sdc_vfc_payload(self, **kwargs):
        """Build SDC vfc payload."""
        try:
            payload = {}
            if kwargs['action'] == "Create":
                payload = onap_utils.get_config(
                    "vfc_payload.vfc_create_data")
                if "vfc_name" in kwargs:
                    payload['name'] = kwargs['vfc_name']
                else:
                    payload['name'] = "ONAP-test-VFC"
            if kwargs['action'] == "Checkin":
                payload = onap_utils.get_config(
                    "vfc_payload.vfc_checkin_data")
            if kwargs['action'] == "Submit_for_testing":
                payload = onap_utils.get_config(
                    "vfc_payload.vfc_submit_for_testing_data")
            if kwargs['action'] == "Start_certification":
                payload = onap_utils.get_config(
                    "vfc_payload.vfc_start_certification_data")
            if kwargs['action'] == "Certify":
                payload = onap_utils.get_config("vfc_payload.vfc_certify_data")
            if kwargs['action'] == "NewVersion":
                payload = onap_utils.get_config(
                    "vfc_payload.new_vfc_version_data")
        except KeyError:
            self.__logger.error("No vfc payload set")
        return payload

    def get_vfc_list(self):
        """Get the list of the VFCs."""
        url = SDC_URL + onap_utils.get_config(
            "onap.sdc.list_vfc_url")
        headers = SDC_HEADERS_DESIGNER
        return self.__send_message_json(
            'GET', 'get vfcs', url, headers,
            exception=onap_test_exceptions.VfcNotFoundException)

    def get_last_version(self, vfc_versions):
        """Get the last version of the VFCs."""
        highest_version = {}
        list_version = []
        for elem in vfc_versions['results']:
            list_version.append(int(elem['name'][:-2]))
        max_version = max(list_version)
        for elem in vfc_versions['results']:
            if int(elem['name'][:-2]) == int(max_version):
                highest_version = deepcopy(elem)
        self.update_vfc(status=highest_version['status'])
        self.update_vfc(version=highest_version['id'])
        self.__logger.debug("VERSION : %s", self.version)

    def check_vfc_exists(self):
        """Check if provided vfc exists in VFC list."""
        vfc_list = self.get_vfc_list()
        vfc_found = False
        for result in vfc_list:
            if result['name'] == self.name:
                vfc_found = True
                self.__logger.info("VFC %s found in VFC list", self.name)
                self.update_vfc(id=result["uuid"])
                self.update_vfc(version=result["version"])
                self.update_vfc(status=result["lifecycleState"])
        if not vfc_found:
            self.__logger.info("VF in not in vf list: %s", self.name)
            self.update_vfc(id="")
            self.update_vfc(unique_id="")
            self.update_vfc(version="")
            self.update_vfc(status="")
        # Get detailed content of created VFC, especially unique_id
        urlpart = onap_utils.get_config(
            "onap.sdc.get_resource_list_url")
        url = SDC_URL2 + urlpart
        headers = SDC_HEADERS_DESIGNER
        response = self.__send_message_json('GET', 'get vfc',
                                            url, headers)
        if response:
            for resource in response["resources"]:
                if resource["uuid"] == self.id:
                    self.__logger.info("VFC resource %s found in VFC list",
                                       resource["name"])
                    self.update_vfc(id=resource["uniqueId"],
                                    vendor_name=resource["vendorName"])

        self.__logger.info("VFC found parameter %s", vfc_found)
        return vfc_found

    def get_vfc_info(self):
        """Get VFC details."""
        vfc_list = self.get_vfc_list()
        for result in vfc_list:
            if result['name'] == self.name:
                self.update_vfc(version=result["version"]["id"])
                self.update_vfc(status=result["status"])
                break

    def create_vfc(self, vendor):
        """Create VFC in SDC (only if it does not exist)."""
        # we check if vfc exists or not, if not we create it
        create_vfc = False
        if not self.check_vfc_exists():
            url = SDC_URL2 + onap_utils.get_config(
                "onap.sdc.create_vfc_url")
            data = self.get_sdc_vfc_payload(action="Create",
                                            vfc_name=self.name)
            data["vendorName"] = vendor.name
            data["vendorId"] = vendor.id
            data["name"] = self.name
            data["tags"][0] = self.name
            conffilename = ("onap_tests/templates/vfc/" + self.name +
                            "_config.yml")
            if os.path.isfile(conffilename):
                self.__logger.info(
                    "upload config yml files to VFC %s", self.name)
                conffile = {'upload': open(conffilename, 'rb')}
                with open(conffilename, 'rb') as conffile:
                    data["payloadData"] = (
                        base64.b64encode(conffile.read()).decode('ascii'))

                # VFC file name to get from variable service_name
                data["payloadName"] = self.service_name + "_config.yml"

                data = json.dumps(data)
                headers = SDC_UPLOAD_HEADERS_VFC
                datachecksum = base64.b64encode(
                    bytes(hashlib.md5(
                        data.encode("utf-8")).hexdigest(), 'utf-8'))
                headers["Content-MD5"] = bytes(datachecksum)
                self.__logger.debug("Create vfc headers : %s", headers)
                self.__logger.debug(
                    "Create vfc md5 : %s", headers["Content-MD5"])
                self.__logger.debug("Create vfc payload : %s", data)
                response = self.__send_message_json('POST', 'create vfc',
                                                    url, headers, data=data)
                if response:
                    create_vfc = True
                    self.update_vfc(unique_id=response["uniqueId"],
                                    vendor_name=response["vendorName"])
                    self.__logger.info(
                        "[SDC][Create vfc] VFC unique Id : %s", self.unique_id)
        else:
            self.__logger.info("[SDC][Create vfc] vfc already exists")
        return create_vfc

    def checkin_vfc(self):
        """Checkin VFC."""
        old_str = onap_utils.get_config("onap.sdc.checkin_vfc_url")
        new_str = old_str.replace("PPvfc_uuidPP", self.unique_id)
        url = SDC_URL2 + new_str
        data = self.get_sdc_vfc_payload(action="Checkin")
        data = json.dumps(data)
        headers = SDC_HEADERS_DESIGNER
        return bool(self.__send_message_json('POST', 'Checkin vfc',
                                             url, headers, data=data))

    def new_vfc_version(self):
        """Upgrade VFC version in SDC."""
        new_vfc_version = False
        old_str = onap_utils.get_config(
            "onap.sdc.new_vfc_version_url")
        new_str = old_str.replace("PPvfc_idPP", self.id)
        old_str = new_str
        new_str = old_str.replace("PPvfc_versionPP", self.version)
        url = SDC_URL2 + new_str
        data = self.get_sdc_vfc_payload(action="NewVersion")
        data = json.dumps(data)
        headers = SDC_HEADERS_DESIGNER
        self.__logger.debug("vfc_id : %s", self.id)
        self.__logger.debug("vfc_version : %s", self.version)
        self.__logger.debug("new_vfc_versionURL : %s", url)
        self.__logger.debug("new_vfc_version payload : %s", data)
        response = self.__send_message_json(
            'POST', 'New version vfc', url, headers, data=data)
        if response:
            new_vfc_version = True
            self.update_vfc(
                status=response['status'],
                version=response['id'])
            self.__logger.debug(
                "[SDC][New version vfc] version id : %s", self.version)
            self.__logger.debug(
                "[SDC][New version vfc] status : %s", self.status)
        return new_vfc_version

    def upload_vfc(self, files):
        """Upload zip for VFC."""
        old_str = onap_utils.get_config("onap.sdc.upload_vfc_url")
        new_str = old_str.replace("PPvfc_idPP", self.unique_id)
        old_str = new_str
        new_str = old_str.replace("PPvfc_versionPP", self.version)
        url = SDC_URL2 + new_str
        headers = SDC_UPLOAD_HEADERS
        return bool(self.__send_message('POST', 'upload vfc',
                                        url, headers, files=files))

    def __action_on_vfc(self, config_path, action, headers):
        """Trigger action on VFc (checkin, checkout, certification)."""
        old_str = onap_utils.get_config(config_path)
        new_str = old_str.replace("PPvfc_uniqueidPP", self.id)
        url = SDC_URL2 + new_str
        data = self.get_sdc_vfc_payload(action=action)
        data = json.dumps(data)
        return bool(self.__send_message_json('POST', "{} vf".format(action),
                                             url, headers, data=data))

    def submit_for_testing_vfc(self):
        """Submit VF for testing."""
        return self.__action_on_vfc("onap.sdc.submit_for_testing_vfc_url",
                                    "Submit_for_testing", SDC_HEADERS_DESIGNER)

    def start_certif_vfc(self):
        """Start the certification of the VFC."""
        return self.__action_on_vfc("onap.sdc.start_certif_vfc_url",
                                    "Start_certification", SDC_HEADERS_TESTER)

    def certify_vfc(self):
        """Certify the VFC."""
        return self.__action_on_vfc("onap.sdc.certify_vfc_url",
                                    "Certify", SDC_HEADERS_TESTER)
