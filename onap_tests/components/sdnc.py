#!/usr/bin/python
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
#  pylint: disable=missing-docstring
#  pylint: disable=no-self-use
"""SDNC class."""
import json
import logging
import requests

import onap_tests.utils.sender
import onap_tests.utils.utils as onap_test_utils

PROXY = onap_test_utils.get_config("general.proxy")
SDNC_HEADERS = onap_test_utils.get_config("onap.sdnc.headers")
SDNC_URL = onap_test_utils.get_config("onap.sdnc.url")
SDNC_PRELOAD_VNFS_URL = "/restconf/config/VNF-API:preload-vnfs"
SDNC_VNF_API = "/restconf/operations/VNF-API"


class Sdnc():
    """SDNC class to manage main operations."""

    __logger = logging.getLogger(__name__)

    def __init__(self):
        """Init Sdnc class."""
        # Logger
        # pylint: disable=no-member
        requests.packages.urllib3.disable_warnings()
        self.__sender = onap_tests.utils.sender.Sender()

    def __send_message(self, method, action, url, header=SDNC_HEADERS,
                       **kwargs):
        return self.__sender.send_message('SDNC', method, action,
                                          url, header, **kwargs)

    def __send_message_json(self, method, action, url, header=SDNC_HEADERS,
                            **kwargs):
        return self.__sender.send_message_json('SDNC', method, action,
                                               url, header, **kwargs)

    def get_preload_list(self):
        """Get Preload List."""
        url = SDNC_URL + SDNC_PRELOAD_VNFS_URL
        self.__logger.debug("SDNC url: %s", url)
        response = self.__send_message('GET', 'get preloads', url)
        self.__logger.info("SDNC response on get preload list: %s",
                           response.text)

    def get_preload_item(self, vnf_name, vnf_type):
        """
        Get Preload items.

        :param vnf_name: the name of the VNF
        :param vnf type: the type of VNF
        """
        url = (SDNC_URL + SDNC_PRELOAD_VNFS_URL +
               "/vnf-preload-list/" + vnf_name + "/" + vnf_type)
        self.__logger.debug("SDNC url: %s", url)
        response = self.__send_message('GET', 'get preload item', url)
        self.__logger.info("SDNC response on get preload  %s",
                           response.text)

    @staticmethod
    def get_preload_payload(vnf_parameters, vnf_topology_identifier):
        """
        Get preload payload.

        :param vnf_parameters: the config parameters of the VNF
        :param vnf topology identifier: VNF topology identifier
        """
        # pylint: disable=anomalous-backslash-in-string
        svc_notif = "http:\/\/onap.org:8080\/adapters\/rest\/SDNCNotify"
        return json.dumps({
            "input": {
                "request-information": {
                    "notification-url": "onap.org",
                    "order-number": "1",
                    "order-version": "1",
                    "request-action": "PreloadVNFRequest",
                    "request-id": "test"},
                "sdnc-request-header": {
                    "svc-action": "reserve",
                    "svc-notification-url": svc_notif,
                    "svc-request-id": "test"},
                "vnf-topology-information": {
                    "vnf-assignments": {
                        "availability-zones": [],
                        "vnf-networks": [],
                        "vnf-vms": []},
                    "vnf-parameters": vnf_parameters,
                    "vnf-topology-identifier": vnf_topology_identifier}
            }})

    @staticmethod
    def get_net_preload_payload(net_topology_identifier,
                                net_provider_info, net_subnets):
        """
        Get Network preload payload.

        :param net_topology_identifier: the id of the network topology
        :param net_provider_info: the net provider info
        :param net_subnets: the subnet infos
        """
        # pylint: disable=anomalous-backslash-in-string
        svc_notif = "http:\/\/onap.org:8080\/adapters\/rest\/SDNCNotify"
        return json.dumps({
            "input": {
                "request-information": {
                    "notification-url": "so.onap.org",
                    "order-number": "1",
                    "order-version": "1.0",
                    "request-action": "PreloadNetworkRequest",
                    "request-sub-action": "SUPP",
                    "request-id": "test"},
                "sdnc-request-header": {
                    "svc-action": "reserve",
                    "svc-notification-url": svc_notif,
                    "svc-request-id": "test"},
                "network-topology-information": {
                    "network-topology-identifier": net_topology_identifier,
                    "provider-network-information": net_provider_info,
                    "subnets": net_subnets}}})

    def preload_net(self, sdnc_preload_payload):
        """
        Preload Network.

        :param sdnc_preload_payload: the sdnc preload payload
        """
        # preload_payload =
        url = SDNC_URL + SDNC_VNF_API + ":preload-network-topology-operation"
        self.__logger.debug("SDNC url: %s", url)
        response = self.__send_message('POST', 'post net preload',
                                       url, data=sdnc_preload_payload)
        self.__logger.info("SDNC response on get preload %s",
                           response.status_code)
        return response.status_code

    def preload(self, sdnc_preload_payload):
        """
        Preload VNF.

        :param sdnc_preload_payload: the sdnc preload payload
        """
        # preload_payload =
        url = SDNC_URL + SDNC_VNF_API + ":preload-vnf-topology-operation"
        self.__logger.debug("SDNC url: %s", url)
        response = self.__send_message('POST', 'post vnf preload',
                                       url, data=sdnc_preload_payload)
        self.__logger.info("SDNC response on get preload %s",
                           response.status_code)
        return response.status_code

    def delete_preload(self, vnf_name, vnf_type):
        """
        Delete Preload.

        :param vnf_name: the VNF name
        :param vnf_type: the VNF type
        """
        url = (SDNC_URL + SDNC_VNF_API +
               ":preload-vnfs/vnf-preload-list/" +
               vnf_name + "/" + vnf_type)
        self.__logger.debug("SDNC url: %s", url)
        response = self.__send_message('DELETE', 'delete preload', url)
        if response:
            res = response.status_code
            self.__logger.info("SDNC response on delete preload: %s",
                               res)
        else:
            res = None
        return res
