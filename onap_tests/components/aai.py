#!/usr/bin/python
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
"""Aai class."""
import logging
import time
import requests

import onap_tests.utils.sender
import onap_tests.utils.exceptions as onap_test_exceptions
import onap_tests.utils.utils as onap_test_utils

PROXY = onap_test_utils.get_config("general.proxy")
AAI_HEADERS = onap_test_utils.get_config("onap.aai.headers")
AAI_URL = onap_test_utils.get_config("onap.aai.url")
AAI_VERSION = "/aai/v13"


class Aai():
    """ONAP AA&I main Class operations."""

    __logger = logging.getLogger(__name__)
    __sender = onap_tests.utils.sender.Sender()

    def __init__(self):
        """Init Aai Class."""
        # pylint: disable=no-member
        requests.packages.urllib3.disable_warnings()

    @classmethod
    def __send_message(cls, method, action, url, header=AAI_HEADERS,
                       **kwargs):
        return cls.__sender.send_message('AAI', method, action,
                                         url, header, **kwargs)

    @classmethod
    def __send_message_json(cls, method, action, url, header=AAI_HEADERS,
                            **kwargs):
        return cls.__sender.send_message_json('AAI', method, action,
                                              url, header, **kwargs)

    def check_service_instance(self, service_description, service_instance_id,
                               customer_name):
        """
        Check that a given service instance is created.

        :param service_description: the description of the service
        :param service_instance_id: the service instance id
        :param customer_name: the customer name
        """
        if customer_name == "EMPTY":
            customer = onap_test_utils.get_config("onap.customer")
        else:
            customer = customer_name
        url = (AAI_URL + "/aai/v11/business/customers/customer/" +
               customer +
               "/service-subscriptions/service-subscription/" +
               service_description + "/service-instances/")
        try:
            service_instance_found = False
            nb_try = 0
            nb_try_max = 5
            while service_instance_found is False and nb_try < nb_try_max:
                response = self.__send_message('GET',
                                               'get service instances', url)
                self.__logger.debug(
                    "AAI: looking for %s service instance....",
                    service_instance_id)
                if response and (service_instance_id in response.text):
                    self.__logger.info(
                        "Service instance %s found in AAI",
                        service_instance_id)
                    service_instance_found = True
                time.sleep(20)
                nb_try += 1
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error("impossible to perform the request on AAI: %s",
                                str(err))
        if service_instance_found is False:
            self.__logger.info("Service instance not found")
            raise onap_test_exceptions.ServiceInstantiateException
        return service_instance_found

    def check_service_instance_cleaned(self, service_description,
                                       service_instance_id, customer_name):
        """
        Check if the Service instance has been  cleaned in the AAI.

        Return True if it has been clean
        False elsewhere
        """
        if customer_name == "EMPTY":
            customer = onap_test_utils.get_config("onap.customer")
        else:
            customer = customer_name

        # url2 = AAI_URL + "/aai/v11/service-design-and-creation/services"
        # url1 = AAI_URL + "/aai/v11/business/customers"
        url = (AAI_URL + "/aai/v11/business/customers/customer/" +
               customer +
               "/service-subscriptions/service-subscription/" +
               service_description + "/service-instances/")

        try:
            service_instance_found = True
            nb_try = 0
            nb_try_max = 5
            while service_instance_found is True and nb_try < nb_try_max:
                response = self.__send_message('GET',
                                               'service instances cleaned',
                                               url)
                self.__logger.debug(
                    "AAI: %s service instance cleaned?", service_instance_id)
                if not response or (service_instance_id not in response.text):
                    self.__logger.info(
                        "Service instance %s cleaned in AAI",
                        service_instance_id)
                    service_instance_found = False
                time.sleep(20)
                nb_try += 1
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error("impossible to perform the request on AAI: %s",
                                str(err))
        if service_instance_found is False:
            self.__logger.info("Service instance cleaned")
        else:
            raise onap_test_exceptions.ServiceInstantiateException

        return not service_instance_found

    def check_vnf_instance(self, vnf_id):
        """
        Check the VNF declared in the A&AI.

        :param vnf_id: the id of the VNF
        """
        url = (AAI_URL + "/aai/v11/network/generic-vnfs")
        try:
            vnf_instance_found = False
            nb_try = 0
            nb_try_max = 5
            while vnf_instance_found is False and nb_try < nb_try_max:
                response = self.__send_message('GET',
                                               'get vnf instances', url)
                self.__logger.debug(
                    "AAI: looking for %s vnf instance....", vnf_id)
                if response and (vnf_id in response.text):
                    self.__logger.info("Vnf instance %s found in AAI", vnf_id)
                    vnf_instance_found = True
                time.sleep(20)
                nb_try += 1
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error("impossible to perform the request on AAI: %s",
                                str(err))
        if vnf_instance_found is False:
            self.__logger.info("VNF instance not found")
            raise onap_test_exceptions.VNFInstantiateException

        return vnf_instance_found

    def check_vnf_instances(self, vnf_ids):
        """
        Check the VNF(s) declared in the A&AI.

        :param vnf_ids: the list of VNF ids
        """
        url = (AAI_URL + AAI_VERSION + "/network/generic-vnfs")
        # if case of several VNFs, answer False if one is not found
        nb_vnf_found = 0
        for elt in vnf_ids:
            try:
                self.__logger.info("AAI: looking for %s vnf instance....", elt)
                vnf_id = vnf_ids[elt]["id"]
                nb_try = 0
                nb_try_max = 5
                vnf_instance_found = False
                while vnf_instance_found is False and nb_try < nb_try_max:
                    response = self.__send_message('GET',
                                                   'get all vnf instances',
                                                   url)
                    if response and (vnf_id in response.text):
                        self.__logger.info(
                            "Vnf instance %s found in AAI", vnf_id)
                        nb_vnf_found += 1
                        vnf_instance_found = True
                    time.sleep(20)
                    nb_try += 1
            except Exception as err:  # pylint: disable=broad-except
                self.__logger.error("Error on AAI request: %s",
                                    str(err))
            if nb_vnf_found == len(vnf_ids):
                self.__logger.info("VNF instance found")
                vnf_instance_found = True
            else:
                vnf_instance_found = False
                self.__logger.error("VNF not found")
                raise onap_test_exceptions.VNFInstantiateException

        return vnf_instance_found

    def check_vnf_cleaned(self, vnf_id):
        """
        Check the VNF declared in the A&AI.

        :param vnf_id: the VNF id
        """
        url = AAI_URL + AAI_VERSION + "/network/generic-vnfs"
        try:
            vnf_instance_found = True
            nb_try = 0
            nb_try_max = 5
            while vnf_instance_found is True and nb_try < nb_try_max:
                response = self.__send_message('GET',
                                               'vnf instances cleaned',
                                               url)
                self.__logger.info(
                    "AAI: check if vnf %s instance is cleaned.", vnf_id)
                if not response or (vnf_id not in response.text):
                    self.__logger.info(
                        "Vnf instance %s cleaned in AAI", vnf_id)
                    vnf_instance_found = False
                time.sleep(20)
                nb_try += 1
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error("impossible to perform the request on AAI: %s",
                                str(err))
        if vnf_instance_found is True:
            self.__logger.info("VNF still in AAI, instance not cleaned")
        return not vnf_instance_found

    def check_net_instance(self, net_id):
        """
        Check the Net declared in the A&AI.

        :param net_id: the network id
        """
        url = (AAI_URL + "/aai/v14/network/l3-networks")
        try:
            net_instance_found = False
            nb_try = 0
            nb_try_max = 30
            while net_instance_found is False and nb_try < nb_try_max:
                response = requests.get(url, headers=AAI_HEADERS,
                                        proxies=PROXY, verify=False)
                self.__logger.info("AAI: looking for %s Network instance....",
                                   net_id)
                if net_id in response.text:
                    self.__logger.info("Network instance %s found in AAI",
                                       net_id)
                    net_instance_found = True
                time.sleep(20)
                nb_try += 1
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error("impossible to perform the request on AAI: %s",
                                str(err))
        if net_instance_found is False:
            self.__logger.info("Network instance not found")
            raise onap_test_exceptions.NetworkInstantiateException
        return net_instance_found

    def check_net_cleaned(self, net_id):
        """
        Check the Net declared in the AA&I has been cleaned.

        :param net_id: the network id
        Return True if it has been cleaned
        False if not
        """
        url = (AAI_URL + "/aai/v14/network/l3-networks")
        try:
            net_instance_found = True
            nb_try = 0
            nb_try_max = 30
            while net_instance_found is True and nb_try < nb_try_max:
                response = requests.get(url, headers=AAI_HEADERS,
                                        proxies=PROXY, verify=False)
                self.__logger.info("AAI: Check if %s Network has been cleaned",
                                   net_id)
                if net_id not in response.text:
                    self.__logger.info("Network instance %s cleaned in AAI",
                                       net_id)
                    net_instance_found = False
                time.sleep(15)
                nb_try += 1
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error("impossible to perform the request on AAI: %s",
                                str(err))
        if net_instance_found is True:
            self.__logger.info("Network still in AAU, instance not cleaned")
        return not net_instance_found

    def check_module_instance(self, vnf_id, module_id):
        """
        Check the VNF declared in the A&AI.

        :param vnf_id: the id of the Vnf
        :param module_id: the id of the module
        """
        self.__logger.debug(">>> check_module_instance")
        url = (AAI_URL + AAI_VERSION + "/network/generic-vnfs/generic-vnf/" +
               vnf_id + "/vf-modules")
        try:
            module_instance_found = False
            nb_try = 0
            nb_try_max = 20
            while module_instance_found is False and nb_try < nb_try_max:
                response = self.__send_message('GET',
                                               'get module instances',
                                               url)
                self.__logger.info(
                    "AAI: looking for %s module instance....", module_id)

                if response and (module_id in response.text):
                    self.__logger.info(
                        "Module instance %s found in AAI", module_id)
                    module_instance_found = True
                else:
                    time.sleep(20)
                    nb_try += 1
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error("impossible to perform the request on AAI: %s",
                                str(err))
        if module_instance_found is False:
            self.__logger.info("VFModule instance not found")
            raise onap_test_exceptions.VfModuleInstantiateException
        return module_instance_found

    def check_module_cleaned(self, vnf_id, module_id):
        """
        Check that the VfModule declared in the A&AI has been cleaned.

        :param vnf_id: the vnf id
        :param module_id: the module id
        return True if it has been cleaned
        False if not
        """
        url = (AAI_URL + AAI_VERSION + "/network/generic-vnfs/generic-vnf/" +
               vnf_id + "/vf-modules")
        try:
            module_instance_found = True
            nb_try = 0
            nb_try_max = 10
            while module_instance_found is True and nb_try < nb_try_max:
                response = self.__send_message('GET',
                                               'module instances cleaned',
                                               url)
                self.__logger.info(
                    "AAI: Check if module  %s has been cleaned", module_id)
                if not response or (module_id not in response.text):
                    self.__logger.info(
                        "Module instance %s cleaned in AAI", module_id)
                    module_instance_found = False
                time.sleep(15)
                nb_try += 1
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error("impossible to perform the request on AAI: %s",
                                str(err))
        if module_instance_found is True:
            self.__logger.info("VFModule still in AAI, instance not cleaned")
        return not module_instance_found

    @classmethod
    def get_customers(cls):
        """Get the list of subscription types in A&AI."""
        url = AAI_URL + AAI_VERSION + "/business/customers"
        return cls.__send_message_json(cls, 'GET', 'get customers', url)

    @classmethod
    def get_subscription_type_list(cls):
        """Get the list of subscription types in A&AI."""
        url = AAI_URL + AAI_VERSION + "/service-design-and-creation/services"
        return cls.__send_message_json(cls, 'GET', 'get subscriptions', url)

    @classmethod
    def get_vnfid_from_instance_id(cls, instance_id):
        """Get the VNF_ID associated with an instance-id."""
        url = (AAI_URL + AAI_VERSION +
               "/nodes/service-instances/service-instance/" +
               instance_id)
        return cls.__send_message_json(cls, 'GET', 'get vnf_id', url)

    @classmethod
    def get_vfmoduleid_from_vnf_id(cls, vnf_id):
        """Get the list of subscription types in AAI."""
        url = (AAI_URL + AAI_VERSION + "/network/generic-vnfs/generic-vnf/" +
               vnf_id + "/vf-modules")
        return cls.__send_message_json(cls, 'GET', 'get module_id', url)

    @classmethod
    def get_infos_from_instance_id(cls, customer_id,
                                   service_type, instance_id):
        """Get the list of subscription types in AAI."""
        url = (AAI_URL + AAI_VERSION + "/business/customers/customer/" +
               customer_id + "/service-subscriptions/service-subscription/" +
               service_type + "/service-instances/service-instance/" +
               instance_id)
        return cls.__send_message_json(cls, 'GET', 'get service infos', url)

    @classmethod
    def put_custom_query(cls, instance_id, query):
        """Get a module from an instance id."""
        url = AAI_URL + AAI_VERSION + "/query?format=simple"
        start = ["business/" + instance_id]
        # "query" : "query/vfModule-fromServiceInstance"}
        data = {"start": start,
                "query": query}
        return cls.__send_message_json(cls, 'PUT',
                                       'put custom query', url, data=data)

    @classmethod
    def get_cloud_regions(cls):
        """Get the list of subscription types in AAI."""
        url = AAI_URL + "/cloud-infrastructure/cloud-regions"
        return cls.__send_message_json(cls, 'GET', 'get regions', url)
