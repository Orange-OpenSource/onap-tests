#!/usr/bin/env python
# coding: utf-8
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
#  pylint: disable=too-many-instance-attributes
#  pylint: disable=too-many-branches
#  pylint: disable=too-many-public-methods
#  pylint: disable=no-self-use

"""InitOnapData class."""

import logging
import json
import os
import sys
import urllib3

import openstack
from openstack.config import loader

import onap_tests.utils.sender
import onap_tests.utils.utils as onap_utils

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

PROXY = onap_utils.get_config("general.proxy")
AAI_URL = onap_utils.get_config("onap.aai.url")
VID_URL = onap_utils.get_config("onap.vid.url")
MULTICLOUD_URL = onap_utils.get_config("onap.multicloud.url")
MULTICLOUD_K8S_URL = onap_utils.get_config("onap.multicloud.k8s_multicloud_url")
AAI_HEADERS = onap_utils.get_config("onap.aai.headers")
VID_HEADERS = onap_utils.get_config("onap.vid.headers")
MULTICLOUD_HEADERS = onap_utils.get_config("onap.multicloud.headers")

TEST_CLOUD = os.getenv('OS_TEST_CLOUD', 'openlab-vnfs-ci')
loader.OpenStackConfig()

class InitOnapData():
    """
    Init ONAP Data.

      * declare a generic vendor in SDC
      * declare a generic customer in AAI
      * declare a Cloud Region and Tenant in AAI
      * declare OwningEntity, Project, Platform and LineOfBusiness in VID
    """

    __logger = logging.getLogger(__name__)

    def __init__(self, **kwargs):
        """Init InitOnapdata class."""
        # pylint: disable=invalid-name
        if "customer_name" in kwargs:
            self.customer_name = kwargs['customer_name']
        else:
            self.customer_name = "generic"
        self.cloud_owner = onap_utils.get_config("openstack.cloud_owner")
        self.cloud_region_id = onap_utils.get_config("openstack.region_id")
        self.tenant_id = onap_utils.get_config("openstack.tenant_id")
        self.tenant_name = onap_utils.get_config("openstack.tenant_name")
        self.complex_name = onap_utils.get_config("openstack.complex_name")
        self.cloud_region_version = onap_utils.get_config("openstack.region_version")
        self.random_uuid = onap_utils.get_config("openstack.random_uuid")
        self.service_name = onap_utils.get_config("onap.service.name")

        try:
            self.cloud_keystone_url = onap_utils.get_config(
                "openstack.keystone_url")
            self.cloud_keystone_user = onap_utils.get_config(
                "openstack.keystone_user")
            self.cloud_keystone_password = onap_utils.get_config(
                "openstack.keystone_password")
        except ValueError:
            self.__logger.warning(
                "keystone param not defined in config: use openstacksdk")
            cloud = openstack.connect(cloud=TEST_CLOUD)
            self.cloud_keystone_url = cloud.config.auth['auth_url']
            self.cloud_keystone_user = cloud.config.auth['username']
            self.cloud_keystone_password = cloud.config.auth['password']

        self.__sender = onap_tests.utils.sender.Sender()

    def __send_message_aai(self, method, action, url, header=AAI_HEADERS,
                           **kwargs):
        return self.__sender.send_message('AAI', method, action,
                                          url, header, **kwargs)

    def __send_message_json_aai(self, method, action, url, header=AAI_HEADERS,
                                **kwargs):
        return self.__sender.send_message_json('AAI', method, action,
                                               url, header, **kwargs)

    def __send_message_vid(self, method, action, url, header=VID_HEADERS,
                           **kwargs):
        return self.__sender.send_message('VID', method, action,
                                          url, header, **kwargs)

    def __send_message_multicloud(self, method, action, url, header=MULTICLOUD_HEADERS,
                                  **kwargs):
        return self.__sender.send_message('MULTICLOUD', method, action,
                                          url, header, **kwargs)

    def __send_message_k8s_multicloud(self, method, action, url, header=MULTICLOUD_HEADERS,
                                      **kwargs):
        return self.__sender.send_message('MULTICLOUD K8S', method, action,
                                          url, header, **kwargs)

    def __send_message_k8s_upload_multicloud(self, method, action, url,
                                             header=None,
                                             **kwargs):
        return self.__sender.send_message('MULTICLOUD K8S', method, action,
                                          url, header, **kwargs)

    def __send_message_json_vid(self, method, action, url, header=VID_HEADERS,
                                **kwargs):
        return self.__sender.send_message_json('VID', method, action,
                                               url, header, **kwargs)

    def update_values(self, **kwargs):
        """Update  values."""
        if "customer_name" in kwargs:
            self.customer_name = kwargs['customer_name']

    def get_aai_service_payload(self, **kwargs):
        """Build AAI service payload."""
        try:
            payload = {}
            if kwargs['action'] == "declare_customer":
                payload = onap_utils.get_config(
                    "service_payload.declare_customer_data")
                if "customer_name" in kwargs:
                    payload['global-customer-id'] = kwargs['customer_name']
                    payload['subscriber-name'] = kwargs['customer_name']
                else:
                    payload['global-customer-id'] = "generic"
                    payload['subscriber-name'] = "generic"

            if kwargs['action'] == "declare_complex":
                payload = onap_utils.get_config(
                    "service_payload.declare_complex_data")
                payload['physical-location-id'] = kwargs['complex_name']
                payload['complex-name'] = kwargs['complex_name']

            if kwargs['action'] == "declare_cloud_region":
                payload = onap_utils.get_config(
                    "service_payload.declare_cloud_region_data")
                payload['cloud-region-id'] = kwargs['cloud_region_id']
                payload['cloud-owner'] = kwargs['cloud_owner']
                payload['cloud-region-version'] = kwargs['cloud_region_version']
                payload['complex-name'] = kwargs['complex_name']
                extra = str(payload['cloud-extra-info'])
                extra = extra.replace(
                    "PPcloud_region_idPP", kwargs['cloud_region_id'])
                payload['cloud-extra-info'] = extra
                payload['esr-system-info-list']['esr-system-info'][0]['esr-system-info-id'] = \
                    kwargs['random_uuid']
                payload['esr-system-info-list']['esr-system-info'][0]['service-url'] = \
                    kwargs['cloud_keystone_url']
                payload['esr-system-info-list']['esr-system-info'][0]['user-name'] = \
                    kwargs['cloud_keystone_user']
                payload['esr-system-info-list']['esr-system-info'][0]['password'] = \
                    kwargs['cloud_keystone_password']
                payload['esr-system-info-list']['esr-system-info'][0]['default-tenant'] = \
                    kwargs['tenant_name']

            if kwargs['action'] == "add_tenant_to_cloud":
                payload = onap_utils.get_config(
                    "service_payload.add_tenant_to_cloud_data")
                payload['tenant-id'] = kwargs['tenant_id']
                payload['tenant-name'] = kwargs['tenant_name']

            if kwargs['action'] == "link_complex_to_cloud":
                payload = onap_utils.get_config(
                    "service_payload.link_complex_to_cloud_data")
                payload['relationship-data'][0]['relationship-value'] = \
                    kwargs['complex_name']
                related_link = str(payload['related-link'])
                related_link = related_link.replace(
                    "PPcomplex_namePP", kwargs['complex_name'])
                payload['related-link'] = related_link

        except KeyError:
            self.__logger.error("No payload set")
        return payload

    def get_vid_payload(self, **kwargs):
        """Build VID payload."""
        try:
            payload = {}
            if kwargs['action'] == "Declare_owningentity":
                payload = onap_utils.get_config(
                    "service_payload.declare_owningentity_data")
            if kwargs['action'] == "Declare_platform":
                payload = onap_utils.get_config(
                    "service_payload.declare_platform_data")
            if kwargs['action'] == "Declare_lineofbusiness":
                payload = onap_utils.get_config(
                    "service_payload.declare_lineofbusiness_data")
            if kwargs['action'] == "Declare_project":
                payload = onap_utils.get_config(
                    "service_payload.declare_project_data")
        except KeyError:
            self.__logger.error("No payload set")
        return payload

    def get_cloud_region(self):
        """Get cloud region list in AAI."""
        url = AAI_URL + onap_utils.get_config(
            "onap.aai.list_cloud_region_url")
        cloud_list = {}
        try:
            response = self.__send_message_aai('GET', 'get clouds', url)
            cloud_list = response.json()
            self.__logger.info(
                "GET Cloud list from AAI code : %s", response.status_code)
            if response.status_code == 404:
                cloud_list = []
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error(
                "Failed to perform List cloud_list in AAI: %s", err)
        return cloud_list

    def get_complex_list(self):
        """Get complex list in AAI."""
        url = AAI_URL + onap_utils.get_config(
            "onap.aai.list_complex_url")
        complex_list = {}
        try:
            response = self.__send_message_aai('GET', 'get complex', url)
            complex_list = response.json()
            self.__logger.info(
                "GET Complex list from AAI code : %s", response.status_code)
            if response.status_code == 404:
                complex_list = []
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error(
                "Failed to perform List complex in AAI: %s", err)
        return complex_list

    def get_customer_list(self):
        """Get customer list in AAI."""
        url = AAI_URL + onap_utils.get_config(
            "onap.aai.list_customer_url")
        customer_list = {}
        try:
            response = self.__send_message_aai('GET', 'get customers', url)
            customer_list = response.json()
            self.__logger.info(
                "GET Customer list from AAI code : %s", response.status_code)
            if response.status_code == 404:
                customer_list = []
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error(
                "Failed to perform List customer in AAI: %s", err)
        return customer_list

    def get_tenant_list(self):
        """Get tenant already declared with that cloud."""
        url_str = onap_utils.get_config("onap.aai.get_tenant_url")
        url_str = url_str.replace("PPcloud_region_idPP", self.cloud_region_id)
        url_str = url_str.replace("PPcloud_ownerPP", self.cloud_owner)
        url = AAI_URL + url_str
        tenant_list = {}
        try:
            response = self.__send_message_aai('GET', 'get tenants', url)
            tenant_list = response.json()
            self.__logger.info(
                "GET Tenant list from AAI code : %s", response.status_code)
            if response.status_code == 404:
                tenant_list = []
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error("Failed to perform\
                 List tenant in AAI: %s", err)
        return tenant_list

    def check_complex_exists(self):
        """Check if complex exists in AAI."""
        self.__logger.info("check if %s exists", self.complex_name)
        complex_list = self.get_complex_list()
        complex_found_in_aai = False
        self.__logger.debug(
            "Complex list from AAI : %s", complex_list)
        if complex_list:
            for result in complex_list['complex']:
                if result['complex-name'] == self.complex_name:
                    complex_found_in_aai = True
        return complex_found_in_aai

    def check_cloud_exists(self):
        """Check if cloud  exists in AAI."""
        self.__logger.info("check if %s exists", self.cloud_region_id)
        cloud_list = self.get_cloud_region()
        cloud_found_in_aai = False
        self.__logger.debug(
            "Cloud list from AAI : %s", cloud_list)
        if cloud_list:
            for result in cloud_list['cloud-region']:
                if (result['cloud-region-id'] == self.cloud_region_id and
                        result['cloud-owner'] == self.cloud_owner):
                    cloud_found_in_aai = True
        return cloud_found_in_aai

    def check_customer_exists(self):
        """Check if customer  exists in AAI."""
        self.__logger.info("check if %s exists", self.customer_name)
        customer_list = self.get_customer_list()
        customer_found_in_aai = False
        self.__logger.debug(
            "Customer list from AAI : %s", customer_list)
        if customer_list:
            for result in customer_list['customer']:
                if result['global-customer-id'] == self.customer_name:
                    customer_found_in_aai = True
        return customer_found_in_aai

    def check_tenant_exists(self):
        """Check if tenant  exists in AAI for a cloud region."""
        self.__logger.info("check if %s exists", self.tenant_id)
        tenant_list = self.get_tenant_list()
        tenant_found_in_aai = False
        self.__logger.debug(
            "Tenant list from AAI : %s", tenant_list)
        if tenant_list:
            for result in tenant_list['tenant']:
                if result['tenant-id'] == self.tenant_id:
                    tenant_found_in_aai = True
        return tenant_found_in_aai

    def declare_customer(self):
        """Declare customer in AAI."""
        declare_customer = False
        url_str = onap_utils.get_config("onap.aai.declare_customer_url")
        url_str = url_str.replace("PPcustomer_namePP", self.customer_name)
        url = AAI_URL + url_str
        data = self.get_aai_service_payload(
            action="declare_customer",
            customer_name=self.customer_name)
        data = json.dumps(data)
        try:
            response = self.__send_message_aai('PUT', 'put customer', url,
                                               data=data)
            if response.status_code == 201:
                declare_customer = True
                self.__logger.info(
                    "declare_customer code : %s", response.status_code)
            else:
                self.__logger.error(
                    "declare_customer code : %s", response.status_code)
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error("Failed to perform\
                 declare_customer : %s", err)
        return declare_customer

    def declare_complex(self):
        """Declare Complex in AAI."""
        declare_complex = False
        url_str = onap_utils.get_config(
            "onap.aai.declare_complex_url")
        url_str = url_str.replace(
            "PPcomplex_namePP", self.complex_name)
        url = AAI_URL + url_str
        data = self.get_aai_service_payload(
            action="declare_complex",
            complex_name=self.complex_name)
        data = json.dumps(data)
        try:
            response = self.__send_message_aai('PUT', 'put complex', url,
                                               data=data)
            if response.status_code == 201:
                declare_complex = True
                self.__logger.info(
                    "declare_complex code : %s", response.status_code)
            else:
                self.__logger.error(
                    "declare_complex code : %s", response.status_code)
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error(
                "Failed to perform declare_complex : %s", err)
        return declare_complex


    def declare_cloud_region(self):
        """Declare Cloud Region in AAI."""
        declare_cloud_region = False
        url_str = onap_utils.get_config(
            "onap.aai.declare_cloud_region_url")
        url_str = url_str.replace(
            "PPcloud_region_idPP", self.cloud_region_id)
        url_str = url_str.replace("PPcloud_ownerPP", self.cloud_owner)
        url = AAI_URL + url_str
        data = self.get_aai_service_payload(
            action="declare_cloud_region",
            cloud_region_id=self.cloud_region_id,
            cloud_owner=self.cloud_owner,
            cloud_region_version=self.cloud_region_version,
            complex_name=self.complex_name,
            random_uuid=self.random_uuid,
            cloud_keystone_url=self.cloud_keystone_url,
            cloud_keystone_user=self.cloud_keystone_user,
            cloud_keystone_password=self.cloud_keystone_password,
            tenant_name=self.tenant_name)
        data = json.dumps(data)
        try:
            response = self.__send_message_aai('PUT', 'put cloud', url,
                                               data=data)
            if response.status_code == 201:
                declare_cloud_region = True
                self.__logger.info(
                    "declare_cloud_region code : %s", response.status_code)
            else:
                self.__logger.error(
                    "declare_cloud_region code : %s", response.status_code)
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error(
                "Failed to perform declare_cloud_region : %s", err)
        return declare_cloud_region

    def link_complex_to_cloud(self):
        """Link Complex to Cloud Region in AAI."""
        link_complex_to_cloud = False
        url_str = onap_utils.get_config(
            "onap.aai.link_complex_to_cloud_url")
        url_str = url_str.replace("PPcloud_region_idPP", self.cloud_region_id)
        url_str = url_str.replace("PPcloud_ownerPP", self.cloud_owner)
        url = AAI_URL + url_str
        data = self.get_aai_service_payload(
            action="link_complex_to_cloud",
            complex_name=self.complex_name)
        data = json.dumps(data)
        self.__logger.info(
            "link_complex_to_cloud url : %s", url)
        self.__logger.info(
            "add_complex_to_cloud payload : %s", data)
        try:
            response = self.__send_message_aai('PUT', 'link complex', url,
                                               data=data)
            if (response.status_code == 201 or response.status_code == 200):
                link_complex_to_cloud = True
                self.__logger.info(
                    "link_complex_to_cloud code : %s", response.status_code)
            else:
                self.__logger.error(
                    "link_complex_to_cloud code : %s", response.status_code)
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error("Failed to perform\
                 link_complex_to_cloud : %s", err)
        return link_complex_to_cloud

    def add_tenant_to_cloud(self):
        """Add Tenant to Cloud Region in AAI."""
        add_tenant_to_cloud = False
        url_str = onap_utils.get_config(
            "onap.aai.add_tenant_to_cloud_url")
        url_str = url_str.replace("PPcloud_region_idPP", self.cloud_region_id)
        url_str = url_str.replace("PPtenant_idPP", self.tenant_id)
        url_str = url_str.replace("PPcloud_ownerPP", self.cloud_owner)
        url = AAI_URL + url_str
        data = self.get_aai_service_payload(
            action="add_tenant_to_cloud",
            tenant_id=self.tenant_id,
            tenant_name=self.tenant_name,
            cloud_owner=self.cloud_owner)
        data = json.dumps(data)
        self.__logger.info(
            "add_tenant_to_cloud url : %s", url)
        self.__logger.info(
            "add_tenant_to_cloud payload : %s", data)
        try:
            response = self.__send_message_aai('PUT', 'put tenant', url,
                                               data=data)
            if response.status_code == 201:
                add_tenant_to_cloud = True
                self.__logger.info(
                    "add_tenant_to_cloud code : %s", response.status_code)
            else:
                self.__logger.error(
                    "add_tenant_to_cloud code : %s", response.status_code)
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error("Failed to perform\
                 add_tenant_to_cloud : %s", err)
        return add_tenant_to_cloud

    def declare_owningentity(self):
        """Declare platfom in VID."""
        declare_owningentity = False
        new_str = onap_utils.get_config(
            "onap.vid.declare_owningentity_url")
        url = VID_URL + new_str
        data = self.get_vid_payload(
            action="Declare_owningentity")
        data = json.dumps(data)
        try:
            response = self.__send_message_vid('POST', 'post owningentity',
                                               url, data=data)
            if response.status_code == 200:
                declare_owningentity = True
                self.__logger.info(
                    "declare_owningentity code : %s", response.status_code)
            else:
                self.__logger.error(
                    "declare_owningentity code : %s", response.status_code)
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error("Failed to perform\
                 declare_owningentity in VID : %s", err)
        return declare_owningentity

    def declare_platform(self):
        """Declare platfom in VID."""
        declare_platform = False
        new_str = onap_utils.get_config(
            "onap.vid.declare_platform_url")
        url = VID_URL + new_str
        data = self.get_vid_payload(action="Declare_platform")
        data = json.dumps(data)
        try:
            response = self.__send_message_vid('POST', 'post platform', url,
                                               data=data)
            if response.status_code == 200:
                declare_platform = True
                self.__logger.info(
                    "declare_platform code : %s", response.status_code)
            else:
                self.__logger.error(
                    "declare_platform code : %s", response.status_code)
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error("Failed to perform\
                 declare_platform in VID : %s", err)
        return declare_platform

    def declare_line_of_business(self):
        """Declare lineOfBusiness in VID."""
        declare_line_of_business = False
        new_str = onap_utils.get_config(
            "onap.vid.declare_lineofbusiness_url")
        url = VID_URL + new_str
        data = self.get_vid_payload(
            action="Declare_lineofbusiness")
        data = json.dumps(data)
        try:
            response = self.__send_message_vid('POST', 'post lineOfBusiness',
                                               url, data=data)
            if response.status_code == 200:
                declare_line_of_business = True
                self.__logger.info(
                    "declare_line_of_business code : %s", response.status_code)
            else:
                self.__logger.error(
                    "declare_line_of_business code : %s", response.status_code)
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error("Failed to perform\
                 declare_line_of_business in VID : %s", err)
        return declare_line_of_business

    def declare_project(self):
        """Declare project in VID."""
        declare_project = False
        new_str = onap_utils.get_config(
            "onap.vid.declare_project_url")
        url = VID_URL + new_str
        data = self.get_vid_payload(action="Declare_project")
        data = json.dumps(data)
        try:
            response = self.__send_message_vid('POST', 'post project', url,
                                               data=data)
            if response.status_code == 200:
                declare_project = True
                self.__logger.info(
                    "declare_project code : %s", response.status_code)
            else:
                self.__logger.error(
                    "declare_project code : %s", response.status_code)
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error("Failed to perform\
                 declare_project in VID : %s", err)
        return declare_project

    def register_cloud_to_multicloud(self):
        """Register Cloud in Multicloud."""
        register_cloud = False
        url_str = onap_utils.get_config(
            "onap.multicloud.register_cloud_url")
        url_str = url_str.replace("PPcloud_region_idPP", self.cloud_region_id)
        url_str = url_str.replace("PPcloud_ownerPP", self.cloud_owner)
        url = MULTICLOUD_URL + url_str
        try:
            response = self.__send_message_multicloud('POST', 'register cloud', url)
            if response.status_code == 202:
                register_cloud = True
                self.__logger.info(
                    "register cloud code : %s", response.status_code)
            else:
                self.__logger.error(
                    "register cloud code : %s", response.status_code)
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error("Failed to perform\
                 register cloud in Multicloud : %s", err)
        return register_cloud

    def get_k8s_rbdef(self, model_info):
        """Get k8s rbdefintion list in multicloud."""
        k8s_rbdef_list = {}
        url_str = onap_utils.get_config(
            "onap.multicloud.k8s_rbdef_metadata")
        url_str = url_str.replace("PPrb_namePP", model_info["modelName"])
        url = MULTICLOUD_K8S_URL + url_str
        try:
            response = self.__send_message_k8s_multicloud('GET', 'k8s rbdefinition list', url)
            self.__logger.info(
                "GET k8s k8s_rbdef_list response from multicloud code : %s", response)
            k8s_rbdef_list = response.json()
            self.__logger.info(
                "GET k8s k8s_rbdef_list list from multicloud code : %s", response.status_code)
            if response.status_code == 404:
                k8s_rbdef_list = []
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error(
                "Failed to get k8s rbdefintion List from multicloud: %s", err)
        return k8s_rbdef_list

    def check_rbdefinition_exists(self, model_info):
        """Check if rbdefinition exists in multicloud."""
        self.__logger.info("check if k8s rbdefinition %s exists",
                           model_info["modelName"])
        rbdef_list = self.get_k8s_rbdef(model_info)
        check_rbdef = False
        self.__logger.debug(
            "K8s rbdefinition list from multicloud : %s", rbdef_list)
        if rbdef_list:
            for result in rbdef_list:
                if result['rb-name'] == model_info["modelName"]:
                    check_rbdef = True
        return check_rbdef

    def create_rbdefinition_metadata(self, model_info):
        """For K8s instantiation, create rbdefinition metadata."""
        create_rbdefinition = False
        url_str = onap_utils.get_config(
            "onap.multicloud.k8s_profile_metadata")
        url_str = url_str.replace("PPrb_namePP", model_info["modelName"])
        url_str = url_str.replace("PPrb_versionPP", model_info["modelVersion"])
        url = MULTICLOUD_K8S_URL + url_str
        payload = {}
        payload["rb-name"] = model_info["modelName"]
        payload["rb-version"] = model_info["modelVersion"]
        payload["chart-name"] = self.service_name
        payload["description"] = "resource bundle definition api"
        payload["labels"] = {}
        payload = json.dumps(payload)
        try:
            response = self.__send_message_k8s_multicloud('POST', 'k8s rbdef', url, data=payload)
            if response.status_code == 201:
                create_rbdefinition = True
                self.__logger.info(
                    "Post Create k8s rbdefinition metadata : %s", response.status_code)
            else:
                self.__logger.error(
                    "Post Create k8s rbdefinition metadata : %s", response.status_code)
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error("Failed to perform\
                 Post k8s rbdefinition metadata : %s", err)
        return create_rbdefinition

    def get_k8s_profile_list(self, model_info):
        """Get k8s profile list in multicloud."""
        k8s_profile_list = {}
        url_str = onap_utils.get_config(
            "onap.multicloud.k8s_profile_metadata")
        url_str = url_str.replace("PPrb_namePP", model_info["modelName"])
        url_str = url_str.replace("PPrb_versionPP", model_info["modelVersion"])
        url = MULTICLOUD_K8S_URL + url_str
        try:
            response = self.__send_message_k8s_multicloud('GET', 'k8s profile list', url)
            self.__logger.info(
                "GET k8s profile list response from multicloud code : %s", response)
            k8s_profile_list = response.json()
            self.__logger.info(
                "GET k8s profile list from multicloud code : %s", response.status_code)
            if response.status_code == 404:
                k8s_profile_list = []
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error(
                "Failed to get k8s profile List from multicloud: %s", err)
        return k8s_profile_list

    def check_k8s_profile_exists(self, model_info):
        """Check if k8s profile exists in multicloud."""
        self.__logger.info("check if k8s profile %s exists",
                           model_info["modelName"])
        profile_list = self.get_k8s_profile_list(model_info)
        check_profile = False
        self.__logger.debug(
            "K8s profile list from multicloud : %s", profile_list)
        if profile_list:
            for result in profile_list:
                if result['profile-name'] == self.service_name:
                    check_profile = True
        return check_profile

    def create_k8s_profile_metadata(self, model_info, namespace):
        """For K8s instantiation, create profile metadata to link rbDefinition to Profile."""
        create_profile = False
        url_str = onap_utils.get_config(
            "onap.multicloud.k8s_profile_metadata")
        url_str = url_str.replace("PPrb_namePP", model_info["modelName"])
        url_str = url_str.replace("PPrb_versionPP", model_info["modelVersion"])
        url = MULTICLOUD_K8S_URL + url_str
        payload = {}
        payload["rb-name"] = model_info["modelName"]
        payload["rb-version"] = model_info["modelVersion"]
        payload["profile-name"] = self.service_name
        payload["namespace"] = namespace
        payload = json.dumps(payload)
        try:
            response = self.__send_message_k8s_multicloud('POST', 'k8s metadata', url, data=payload)
            if response.status_code == 201:
                create_profile = True
                self.__logger.info(
                    "Post Create k8s profile metadata : %s", response.status_code)
            else:
                self.__logger.error(
                    "Post Create k8s profile metadata : %s", response.status_code)
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error("Failed to perform\
                 Post k8s profile metadata : %s", err)
        return create_profile

    def upload_k8s_profile_content(self, model_info):
        """For K8s instantiation, update profile content to link rbDefinition to Profile."""
        upload_profile = False
        url_str = onap_utils.get_config(
            "onap.multicloud.k8s_profile_upload")
        url_str = url_str.replace("PPrb_namePP", model_info["modelName"])
        url_str = url_str.replace("PPrb_versionPP", model_info["modelVersion"])
        url_str = url_str.replace("PPprofile_namePP", self.service_name)
        url = MULTICLOUD_K8S_URL + url_str

        self.__logger.info("upload profile gz file")
        profile_file_path = (sys.path[-1] +
                             "/onap_tests/templates/k8s_profile/k8sprof.tar.gz")
        data = open(profile_file_path, 'rb').read()
        try:
            response = self.__send_message_k8s_upload_multicloud('POST',
                                                                 'k8s profile content',
                                                                 url,
                                                                 data=data)
            if response.status_code == 200:
                upload_profile = True
                self.__logger.info(
                    "Post Create k8s profile content : %s", response.status_code)
            else:
                self.__logger.error(
                    "Post Create k8s profile content : %s", response.status_code)
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error("Failed to perform\
                 Post k8s profile content : %s", err)
        return upload_profile
