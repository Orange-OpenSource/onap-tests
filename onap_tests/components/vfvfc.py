#!/usr/bin/env python
# coding: utf-8
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
#  pylint: disable=missing-docstring
#  pylint: disable=too-many-instance-attributes
"""VFVFC class used for onboarding."""
import logging
import json
import datetime
import base64
import hashlib
import yaml

import onap_tests.utils.sender
import onap_tests.utils.utils as onap_utils

PROXY = onap_utils.get_config("general.proxy")
SDC_URL = onap_utils.get_config("onap.sdc.url")
SDC_URL2 = onap_utils.get_config("onap.sdc.url2")
SDC_HEADERS = onap_utils.get_config("onap.sdc.headers")
SDC_HEADERS_DESIGNER = SDC_HEADERS.copy()
SDC_HEADERS_DESIGNER["USER_ID"] = "cs0008"
SDC_HEADERS_TESTER = SDC_HEADERS.copy()
SDC_HEADERS_TESTER["USER_ID"] = "jm0007"
SDC_UPLOAD_HEADERS = onap_utils.get_config("onap.sdc.upload_headers")
SDC_UPLOAD_HEADERS_VFC = onap_utils.get_config("onap.sdc.vfc_upload_headers")


class Vfvfc():
    """ONAP VF_fromvfc Object used for SDC operations."""

    __logger = logging.getLogger(__name__)

    def __init__(self, **kwargs):
        """Init VFVFC Class."""
        # pylint: disable=invalid-name
        # init for Vfvfc
        self.id = ""
        self.unique_id = ""
        if "vfc_vf_name" in kwargs:
            self.name = kwargs['vfc_vf_name']
        else:
            self.name = ""
        self.version = ""
        self.status = ""
        self.posx = 200
        self.posy = 200
        self.__sender = onap_tests.utils.sender.Sender()

    def __send_message(self, method, action, url, header, **kwargs):
        transact_id = onap_utils.get_uuid()
        header.update(
            {"X-TransactionId": transact_id})
        return self.__sender.send_message('SDC', method, action,
                                          url, header, **kwargs)

    def __send_message_json(self, method, action, url, header, **kwargs):
        transact_id = onap_utils.get_uuid()
        header.update(
            {"X-TransactionId": transact_id})
        return self.__sender.send_message_json('SDC', method, action,
                                               url, header, **kwargs)

    def update_vf(self, **kwargs):
        """Update VF values."""
        # update for Vfvfc
        if "id" in kwargs:
            self.id = kwargs['id']
        if "unique_id" in kwargs:
            self.unique_id = kwargs['unique_id']
        if "name" in kwargs:
            self.name = kwargs['name']
        # VFvfc init
        if "version" in kwargs:
            self.version = kwargs['version']
        if "status" in kwargs:
            self.status = kwargs['status']

    def get_sdc_vf_payload(self, **kwargs):
        """Build SDC VF payload."""
        # Get config from onap_testing.yaml
        try:
            payload = {}
            if kwargs['action'] == "Create":
                payload = onap_utils.get_config(
                    "vf_payload.vf_vfc_create_data")
                if "vendor_name" and "vfc_name" and "vf_name" in kwargs:
                    payload['vendorName'] = kwargs['vendor_name']
                    payload['tags'][0] = kwargs['vf_name']
                    payload['name'] = kwargs['vf_name']
            if kwargs['action'] == "addVFC":
                payload = onap_utils.get_config(
                    "vf_payload.vf_addVFC")
                if "vfc_name" and "vfc_id" in kwargs:
                    payload['componentUid'] = kwargs['vfc_id']
                    payload['name'] = kwargs['vfc_name']
                    dtime = datetime.datetime.now()
                    payload['uniqueId'] = (
                        kwargs['vfc_id'] + dtime.strftime("%s"))
            if kwargs['action'] == "declare_properties_vfvfc":
                payload = onap_utils.get_config(
                    "vf_payload.vf_attributes")
                self.__logger.debug("payload value initial: %s", payload)
                payloadstring = str(payload)
                payloadstring = payloadstring.replace(
                    "PPvf_uniqueidPP", kwargs['vf_uniqueid'])
                payloadstring = payloadstring.replace(
                    "PPvfc_CertidPP", kwargs['vfc_certid'])
                payloadstring = payloadstring.replace(
                    "PPvfc_uniqueidPP", kwargs['vfc_uniqueid'])
                payloadstring = payloadstring.replace(
                    "PPvfcnamePP", kwargs['vfc_name'])
                payloadstring = payloadstring.replace(
                    "PPvfnamePP", kwargs['vf_name'])
                payloadstring = payloadstring.replace("None", "null")
                self.__logger.debug("payload value inter: %s", payloadstring)
                payload = yaml.load(payloadstring)
                self.__logger.debug("payload value final: %s", payload)
            if kwargs['action'] == "vf_getInputs":
                payload = onap_utils.get_config("vf_payload.vf_getInputs")
            if kwargs['action'] == "VFCheckin":
                payload = onap_utils.get_config("vf_payload.vf_checkin_data")
            if kwargs['action'] == "VFSubmit_for_testing":
                payload = onap_utils.get_config(
                    "vf_payload.vf_submit_for_testing_data")
            if kwargs['action'] == "VFStart_certification":
                payload = onap_utils.get_config(
                    "vf_payload.vf_start_certification_data")
            if kwargs['action'] == "VFCertify":
                payload = onap_utils.get_config(
                    "vf_payload.vf_certify_data")
        except KeyError:
            self.__logger.error("No payload set")
        return payload

    def get_vfvfc_list(self):
        """Get vf list."""
        url = SDC_URL + onap_utils.get_config(
            "onap.sdc.list_vf_url")
        headers = SDC_HEADERS_DESIGNER
        return self.__send_message_json('GET', 'get vfvfcs',
                                        url, headers)

    def get_vf_metadata(self):
        """Get VFVFC metadata to get vf unique_id when version is changing."""
        vfvfc_unique_id_updated = False
        old_str = onap_utils.get_config("onap.sdc.metadata_vf_url")
        self.__logger.debug("Test valueVF unique Id : %s", self.unique_id)
        if self.unique_id:
            new_str = old_str.replace("PPvf_unique_idPP", self.unique_id)
            url = SDC_URL2 + new_str
            headers = SDC_HEADERS_DESIGNER
            response = self.__send_message_json('GET', 'get vfcvf metadata',
                                                url, headers)
            if response:
                vfvfc_unique_id_updated = True
                self.update_vf(
                    unique_id=response["metadata"]["allVersions"]["1.0"])
            self.__logger.info("[SDC][get vfcvf metadata] VF unique Id : %s",
                               self.unique_id)
        return vfvfc_unique_id_updated

    def check_vf_exists(self):
        """Check if provided vf exists in vf list."""
        vf_list = self.get_vfvfc_list()
        vf_found = False
        for result in vf_list:
            if result['name'] == self.name:
                vf_found = True
                self.__logger.info("VFvfc %s found in VF list", self.name)
                self.update_vf(id=result["uuid"])
                self.update_vf(version=result["version"])
                self.update_vf(status=result["lifecycleState"])
        if not vf_found:
            self.__logger.info("VFvfc in not in vf list: %s", self.name)
            self.update_vf(id="")
            self.update_vf(unique_id="")
            self.update_vf(version="")
            self.update_vf(status="")
        if vf_found:
            # Get detailed content of created VFvfc, especially unique_id
            urlpart = onap_utils.get_config(
                "onap.sdc.get_resource_list_url")
            url = SDC_URL2 + urlpart
            headers = SDC_HEADERS_DESIGNER
            response = self.__send_message_json('GET', 'get vfcvf',
                                                url, headers)
            if response:
                for resource in response["resources"]:
                    if resource["uuid"] == self.id:
                        self.__logger.info(
                            "VFvfc resource %s found in resource list",
                            resource["name"])
                        self.update_vf(unique_id=resource["uniqueId"],
                                       vendor_name=resource["vendorName"])
            else:
                self.__logger.error(
                    "Get VFvfc resource list status code : %s",
                    response.status_code)

            self.__logger.info("VFvfc found parameter %s", vf_found)

        return vf_found

    def create_vfvfc(self, vfc):
        """Create vf in SDC (only if it does not exist)."""
        # we check if vf exists or not, if not we create it
        create_vfvfc = False
        if not self.check_vf_exists():
            url = SDC_URL2 + onap_utils.get_config(
                "onap.sdc.create_vf_url")
            data = self.get_sdc_vf_payload(action="Create",
                                           vf_name=self.name,
                                           vendor_name=vfc.vendor_name,
                                           vfc_name=vfc.name)
            data = json.dumps(data)
            headers = SDC_UPLOAD_HEADERS_VFC
            datachecksum = base64.b64encode(
                bytes(hashlib.md5(data.encode("utf-8")).hexdigest(), 'utf-8'))
            headers["Content-MD5"] = bytes(datachecksum)
            response = self.__send_message_json('POST', 'Create vfcvf',
                                                url, headers, data=data)
            if response:
                create_vfvfc = True
                vfunc = response.json()
                self.update_vf(id=vfunc["uuid"])
                self.update_vf(unique_id=vfunc["uniqueId"])
                self.__logger.info(
                    "[SDC][Create vfcvf] VFvfc Id : %s", self.id)
                self.__logger.info(
                    "[SDC][Create vfcvf] VFvfc unique Id : %s", self.unique_id)
        return create_vfvfc

    def addvfcvf(self, vfc):
        """Add VFC to vf in SDC (only if it does not exist)."""
        old_str = onap_utils.get_config("onap.sdc.addVFC_vf_url")
        new_str = old_str.replace("PPvf_uniqueidPP", self.unique_id)
        url = SDC_URL2 + new_str
        data = self.get_sdc_vf_payload(action="addVFC",
                                       vfc_name=vfc.name,
                                       vfc_id=vfc.id)
        data = json.dumps(data)
        headers = SDC_HEADERS_DESIGNER
        return bool(self.__send_message_json('POST', 'Add VFC to VF',
                                             url, headers, data=data))

    def declarepropertiesvf(self, vfc):
        """Declare properties assignments for vf in SDC."""
        old_str = onap_utils.get_config(
            "onap.sdc.declare_properties_vf_url")
        new_str = old_str.replace("PPvf_uniqueidPP", self.unique_id)
        url = SDC_URL2 + new_str
        data = self.get_sdc_vf_payload(action="declare_properties_vfvfc",
                                       vf_name=self.name,
                                       vfc_name=vfc.name.lower(),
                                       vf_uniqueid=self.unique_id,
                                       vfc_certid=vfc.id,
                                       vfc_uniqueid=vfc.unique_id)
        data = json.dumps(data)
        headers = SDC_HEADERS_DESIGNER
        return bool(self.__send_message_json('POST',
                                             'Declare assign properties to VF',
                                             url, headers, data=data))

    def extractsubuniqueid(self, response, inputs):
        """Extract the ubunique id."""
        # Goal is to replace PPownerIdPP in the inputs list using the name
        # as searchkey

        output = []
        for entry in inputs:
            for resp in response:
                if entry["name"] == resp["name"]:
                    ownerid = resp["ownerId"]
                    self.__logger.debug(
                        "Substitute identification ownerid: %s", ownerid)
                    resp["defaultValue"] = entry["defaultValue"]
                    output.append(resp)
        self.__logger.debug(
            "Substitute identification output: %s", output)
        return output

    def modifyinputs(self):
        """Modify inputs."""
        payload = onap_utils.get_config("vf_payload.vf_write_inputs")
        self.__logger.debug(
            "[SDC][Modify inputs] vf payload: %s", payload)
        if payload:
            # Perform get requests to get specific ownerId and overwrite
            # inputs to set for VF
            old_str = onap_utils.get_config("onap.sdc.getInputs_vf_url")
            new_str = old_str.replace("PPvf_uniqueidPP", self.unique_id)
            url = SDC_URL2 + new_str
            headers = SDC_HEADERS_DESIGNER
            response = self.__send_message_json('GET', 'get input',
                                                url, headers)
            if response:
                inputstoset = self.extractsubuniqueid(response["inputs"],
                                                      payload)
                self.__logger.info(
                    "[SDC][get input] Assignement inputs to modify for vf: %s",
                    inputstoset)
            # Set modified inputs for VF
            old_str = onap_utils.get_config("onap.sdc.modifyInputs_vf_url")
            new_str = old_str.replace("PPvf_uniqueidPP", self.unique_id)
            url = SDC_URL2 + new_str
            data = json.dumps(inputstoset)
            headers = SDC_HEADERS_DESIGNER
            return bool(self.__send_message_json('POST',
                                                 'Modify inputs for VF',
                                                 url, headers, data=data))

        self.__logger.info("No inputs to modify for vf")
        return False

    def upgrade_vfvfc(self, vfc):
        """Upgrade vf in SDC."""
        upgrade_vfvfc = False
        url = SDC_URL2 + onap_utils.get_config(
            "onap.sdc.upgrade_vf_url")
        data = self.get_sdc_vf_payload(action="Upgrade",
                                       vf_name=self.name,
                                       vendor_name=vfc.vendor_name,
                                       vfc_name=vfc.name,
                                       csar_uuid=vfc.csar_uuid)
        data = json.dumps(data)
        headers = SDC_HEADERS_DESIGNER
        response = self.__send_message_json('POST', 'Upgrade vfcvf',
                                            url, headers, data=data)
        if response:
            upgrade_vfvfc = True
            self.update_vf(id=response["uuid"])
            self.update_vf(unique_id=response["uniqueId"])
            self.__logger.info(
                "[SDC][Upgrade vfcvf] VFvfc Id : %s", self.id)
            self.__logger.info(
                "[SDC][Upgrade vfcvf] VFvfc unique Id : %s", self.unique_id)

        return upgrade_vfvfc

    def get_vf_info(self, vfunction):
        """Get vf detail."""
        vf_list = self.get_vfvfc_list()
        for result in vf_list:
            if result['name'] == vfunction['name']:
                vfunction = result
                break
        return vfunction

    def __action_on_vf(self, config_path, action, headers):
        """Trigger action onVF (checkin, checkout, certification)."""
        old_str = onap_utils.get_config(config_path)
        new_str = old_str.replace("PPvf_uuidPP", self.id)
        url = SDC_URL + new_str
        data = self.get_sdc_vf_payload(action=action)
        data = json.dumps(data)
        return bool(self.__send_message_json('POST', "{} vfcvf".format(action),
                                             url, headers, data=data))

    def checkin_vfvfc(self):
        """Checkin VFVFC."""
        return self.__action_on_vf("onap.sdc.checkin_vf_url", "VFCheckin",
                                   SDC_HEADERS_DESIGNER)

    def checkout_vfvfc(self):
        """Checkout VFVFC."""
        checkout_vfvfc = False
        old_str = onap_utils.get_config("onap.sdc.checkout_vf_url")
        new_str = old_str.replace("PPvf_uuidPP", self.unique_id)
        url = SDC_URL2 + new_str
        headers = SDC_HEADERS_DESIGNER
        self.__logger.debug("Checkout url : %s", url)
        self.__logger.debug("Checkout headers : %s", headers)
        response = self.__send_message_json('POST',
                                            "Checkout vfcvf",
                                            url, headers)
        if response:
            checkout_vfvfc = True
            # only working with 1.0 version
            self.update_vf(unique_id=response["uniqueId"])
        return checkout_vfvfc

    def submit_for_testing_vfvfc(self):
        """Submit VFVFC for testing."""
        return self.__action_on_vf("onap.sdc.submit_for_testing_vf_url",
                                   "VFSubmit_for_testing",
                                   SDC_HEADERS_DESIGNER)

    def start_certif_vfvfc(self):
        """Start the certification of the VFVFC."""
        return self.__action_on_vf("onap.sdc.start_certif_vf_url",
                                   "VFStart_certification", SDC_HEADERS_TESTER)

    def certify_vfvfc(self):
        """Certify the VFVFC."""
        return self.__action_on_vf("onap.sdc.certify_vf_url",
                                   "VFCertify", SDC_HEADERS_TESTER)
