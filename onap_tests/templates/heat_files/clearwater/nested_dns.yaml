# Project Clearwater - IMS in the Cloud
# Copyright (C) 2015  Metaswitch Networks Ltd
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version, along with the "Special Exception" for use of
# the program along with SSL, set forth below. This program is distributed
# in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details. You should have received a copy of the GNU General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
#
# The author can be reached by email at clearwater@metaswitch.com or by
# post at Metaswitch Networks Ltd, 100 Church St, Enfield EN2 6BQ, UK
#
# Special Exception
# Metaswitch Networks Ltd  grants you permission to copy, modify,
# propagate, and distribute a work formed by combining OpenSSL with The
# Software, or a work derivative of such a combination, even if such
# copying, modification, propagation, or distribution would otherwise
# violate the terms of the GPL. You must comply with the GPL in all
# respects for all of the code used other than OpenSSL.
# "OpenSSL" means OpenSSL toolkit software distributed by the OpenSSL
# Project and licensed under the OpenSSL Licenses, or a work based on such
# software and licensed under the OpenSSL Licenses.
# "OpenSSL Licenses" means the OpenSSL License and Original SSLeay License
# under which the OpenSSL Project distributes the OpenSSL toolkit software,
# as those licenses appear in the file LICENSE-OPENSSL.

heat_template_version: 2014-10-16

description: >
  DNS server exposing dynamic DNS using DNSSEC

parameters:
  vnf_name:
    type: string
    label: VNF ID
    description: The VNF name provided by ONAP
  vnf_id:
    type: string
    label: VNF ID
    description: The VNF ID provided by ONAP
  vf_module_id:
    type: string
    label: VNF module ID
    description: The VNF module ID provided by ONAP
  public_net_name:
    type: string
    description: ID of public network
    constraints:
      - custom_constraint: neutron.network
        description: Must be a valid network ID
  dns_name_0:
      type: string
      description: Name of server to use
  dns_flavor_name:
    type: string
    description: Flavor to use
    constraints:
      - custom_constraint: nova.flavor
        description: Must be a valid flavor name
  dns_image_name:
    type: string
    description: Name of image to use
  key_name:
    type: string
    description: Name of keypair to assign
    constraints:
      - custom_constraint: nova.keypair
        description: Must be a valid keypair name
  zone:
    type: string
    description: DNS zone
  dnssec_key:
    type: string
    description: DNSSEC private key (Base64-encoded)

resources:

  dns_random_str:
    type: OS::Heat::RandomString
    properties:
      length: 4

  dns_security_group:
    type: OS::Neutron::SecurityGroup
    properties:
      description: security group
      name:
        str_replace:
          template: pre_base_rand
          params:
            pre: dns_sg_
            base: { get_param: vnf_name }
            rand: { get_resource: dns_random_str }
      rules: [
        {remote_ip_prefix: 0.0.0.0/0, protocol: tcp, port_range_min: 22, port_range_max: 22},
        {remote_ip_prefix: 0.0.0.0/0, protocol: tcp, port_range_min: 53, port_range_max: 53},
        {remote_ip_prefix: 0.0.0.0/0, protocol: udp, port_range_min: 53, port_range_max: 53},
        {remote_ip_prefix: 0.0.0.0/0, protocol: icmp}]

  dns_0_public_port_0:
    type: OS::Neutron::Port
    properties:
      name:
        str_replace:
          template: base_rand
          params:
            base: { get_param: vnf_name }
            rand: { get_resource: dns_random_str }
      network: { get_param: public_net_name }
      security_groups: [{ get_resource: dns_security_group }]

  dns_server_0:
    type: OS::Nova::Server
    properties:
      name: { get_param: dns_name_0 }
      image: { get_param: dns_image_name }
      flavor: { get_param: dns_flavor_name }
      key_name: { get_param: key_name }
      networks:
        - port: { get_resource: dns_0_public_port_0 }
      metadata: {vnf_id: { get_param: vnf_id }, vf_module_id: { get_param: vf_module_id }, vnf_name: { get_param: vnf_name }}
      user_data_format: RAW
      user_data:
        str_replace:
          params:
            __zone__: { get_param: zone }
            __dnssec_key__: { get_param: dnssec_key }
          template: |
            #!/bin/bash

            # Log all output to file.
            exec > >(tee -a /var/log/clearwater-heat-dns.log) 2>&1
            set -x

            # Install BIND.
            apt-get update
            DEBIAN_FRONTEND=noninteractive apt-get install bind9 --yes

            # Get the IP address from eth0
            sudo apt-get install ipcalc
            ADDR=`ip addr show eth0 | awk '/inet /{print $2}'`
            PUBLIC_ADDR=`ipcalc -n -b $ADDR | awk '/Address:/{print $2}'`

            # Update BIND configuration with the specified zone and key.
            cat >> /etc/bind/named.conf.local << EOF
            key __zone__. {properties
              algorithm "HMAC-MD5";
              secret "__dnssec_key__";
            };

            zone "__zone__" IN {
              type master;
              file "/var/lib/bind/db.__zone__";
              allow-update {
                key __zone__.;
              };
            };
            EOF

            # Function to give DNS record type and IP address for specified IP address
            ip2rr() {
              if echo $1 | grep -q -e '[^0-9.]' ; then
                echo AAAA $1
              else
                echo A $1
              fi
            }

            # Create basic zone configuration.
            cat > /var/lib/bind/db.__zone__ << EOF
            \$ORIGIN __zone__.
            \$TTL 1h
            @ IN SOA ns admin\@__zone__. ( $(date +%Y%m%d%H) 1d 2h 1w 30s )
            @ NS ns
            ns $(ip2rr $PUBLIC_ADDR)
            EOF
            chown root:bind /var/lib/bind/db.__zone__

            # Now that BIND configuration is correct, kick it to reload.
            service bind9 reload


outputs:
  dns_ip:
    description: IP address of DNS server
    value: { get_attr: [ dns_server_0, networks, { get_param: public_net_name }, 0 ] }
  zone:
    description: DNS zone
    value: { get_param: zone }
  dnssec_key:
    description: DNSSEC private key (Base64-encoded)
    value: { get_param: dnssec_key }
